package com.gmail.val59000mc.winteriscoming.maploader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.World;
import org.bukkit.WorldCreator;

import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.winteriscoming.WIC;
import com.gmail.val59000mc.winteriscoming.configuration.Config;
import com.gmail.val59000mc.winteriscoming.i18n.I18n;


public class MapLoader {
	
	private static boolean deleteFile(File file) {

	    File[] flist = null;

	    if(file == null){
	        return false;
	    }

	    if (file.isFile()) {
	        return file.delete();
	    }

	    if (!file.isDirectory()) {
	        return false;
	    }

	    flist = file.listFiles();
	    if (flist != null && flist.length > 0) {
	        for (File f : flist) {
	            if (!deleteFile(f)) {
	                return false;
	            }
	        }
	    }

	    return file.delete();
	}
	
	private static void copyWorld(String sourcePath, String destPath) {
		Logger.debug("-> MapLoader::copyWorld, sourchePath="+sourcePath+", destPath="+destPath);
		File sourceDir = new File(sourcePath);
		if(sourceDir.exists() && sourceDir.isDirectory()){
			recursiveCopy(sourceDir,new File(destPath));
		}else{
			Logger.severeC(I18n.get("map-loader.copy.not-found"));
		}
		Logger.debug("<- MapLoader::copyWorld");
	}
	
	private static void recursiveCopy(File fSource, File fDest) {
	     try {
	          if (fSource.isDirectory()) {
	          // A simple validation, if the destination is not exist then create it
	               if (!fDest.exists()) {
	                    fDest.mkdirs();
	               }
	 
	               // Create list of files and directories on the current source
	               // Note: with the recursion 'fSource' changed accordingly
	               String[] fList = fSource.list();
	 
	               for (int index = 0; index < fList.length; index++) {
	                    File dest = new File(fDest, fList[index]);
	                    File source = new File(fSource, fList[index]);
	 
	                    // Recursion call take place here
	                    recursiveCopy(source, dest);
	               }
	          }
	          else {
	               // Found a file. Copy it into the destination, which is already created in 'if' condition above
	 
	               // Open a file for read and write (copy)
	               FileInputStream fInStream = new FileInputStream(fSource);
	               FileOutputStream fOutStream = new FileOutputStream(fDest);
	 
	               // Read 2K at a time from the file
	               byte[] buffer = new byte[2048];
	               int iBytesReads;
	 
	               // In each successful read, write back to the source
	               while ((iBytesReads = fInStream.read(buffer)) >= 0) {
	                    fOutStream.write(buffer, 0, iBytesReads);
	               }
	 
	               // Safe exit
	               if (fInStream != null) {
	                    fInStream.close();
	               }
	 
	               if (fOutStream != null) {
	                    fOutStream.close();
	               }
	          }
	     }
	     catch (Exception ex) {
	          // Please handle all the relevant exceptions here
	     }
	}
	
	/**
	 * Create a new world
	 */
	private static void createNewWorld(){
		Logger.debug("-> MapLoader::createNewWorld");

		copyWorld("winteriscoming",Config.worldName);
		
		WIC.getPlugin().getConfig().set("world.last-world", Config.worldName);
		WIC.getPlugin().saveConfig();
		
		Bukkit.getServer().createWorld(new WorldCreator(Config.worldName));
		Logger.debug("<- MapLoader::createNewWorld");
	}

	
	private static void deleteLastWorld(){
		Logger.debug("-> MapLoader::deleteLastWorld");
		String name = Config.lastWorldName;
		
		if(name == null || name.equals("null")){
			// No last world to delete
			Logger.info(I18n.get("map-loader.delete.no-last-world"));
		}else{
			File worldDir = new File(name);
			if(worldDir.exists()){
				// Last world to delete found
				Logger.info(I18n.get("map-loader.delete")+" "+name);
				deleteFile(worldDir);
			}else{
				// Last world to delete not found
				Logger.info(I18n.get("map-loader.delete.last-world-not-found"));
			}
		}
		Logger.debug("<- MapLoader::deleteLastWorld");
	}
	
	private static void loadLastWorld(){
		Logger.debug("-> MapLoader::loadLastWorld, lastWorld="+Config.lastWorldName);
		String name = Config.lastWorldName;
		
		if(name == null || name.equals("null")){
			// No last world name in the config
			Logger.warnC(I18n.get("map-loader.load.no-last-world"));
			deleteLastWorld();
			createNewWorld();
		}else{
			File worldDir = new File(name);
			if(worldDir.exists()){
				// World found
				Bukkit.getServer().createWorld(new WorldCreator(name));
				Config.worldName = Config.lastWorldName;
			}else{
				// World not found
				Logger.warnC(I18n.get("map-loader.load.last-world-not-found"));
				deleteLastWorld();
				createNewWorld();
			}
		}
		Logger.debug("<- MapLoader::loadLastWorld");
	}

	
	public static void deleteOldPlayersFiles() {
		Logger.debug("-> MapLoader::deleteOldPlayersFiles");
		if(Bukkit.getServer().getWorlds().size()>0){
			// Deleting old players files
			File playersData = new File(Bukkit.getServer().getWorlds().get(0).getName()+"/playerdata");
			deleteFile(playersData);
			playersData.mkdirs();
			
			// Deleting old players stats
			File playersStats = new File(Bukkit.getServer().getWorlds().get(0).getName()+"/stats");
			deleteFile(playersStats);
			playersStats.mkdirs();
		}
		Logger.debug("<- MapLoader::deleteOldPlayersFiles");
	}
	
	private static void setupWorldGamerules(){
		Logger.debug("-> MapLoader::setupWorldGamerules");

		World world = Bukkit.getWorld(Config.worldName);
		world.save();
		world.setGameRuleValue("doDaylightCycle", "false");
		world.setGameRuleValue("commandBlockOutput", "false");
		world.setGameRuleValue("logAdminCommands", "false");
		world.setGameRuleValue("sendCommandFeedback", "false");
		world.setGameRuleValue("doMobSpawning", "false");
		world.setGameRuleValue("doTileDrops", "true");
		world.setGameRuleValue("doFireTick", "false");
		
		if(Config.map.equals("game-of-thrones")){
			world.setStorm(true);
			world.setWeatherDuration(999999999);
		}else{
			world.setStorm(false);
			world.setWeatherDuration(999999999);
		}		
		world.setTime(18000);
		world.setDifficulty(Difficulty.HARD);
		world.setWeatherDuration(999999999);
		world.setSpawnLocation(Config.lobbyLocation.getBlockX(), Config.lobbyLocation.getBlockY(), Config.lobbyLocation.getBlockZ());

		Logger.debug("<- MapLoader::setupWorldGamerules");
	}

	public static void load() {
		Logger.debug("-> MapLoader::load");
		if(Config.isLoadLastWord){
			loadLastWorld();
		}else{
			deleteLastWorld();
			createNewWorld();
		}
		Config.loadWorldLocations();
		setupWorldGamerules();
		Logger.debug("<- MapLoader::load");
	}
}
