package com.gmail.val59000mc.winteriscoming.tutorial;

import org.bukkit.entity.Player;

import com.gmail.val59000mc.simpleinventorygui.SIG;
import com.gmail.val59000mc.simpleinventorygui.api.SIGApi;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.winteriscoming.configuration.Config;

public class SIGTutorial {
	
	private static SIGApi api;
	
	public static void load(){
		api = SIG.getPlugin().getAPI();
	}
	
	public static void giveTutorialInventory(Player player){
		Logger.debug("-> SIGTutorial::giveTutorialInventory,player="+player.getName());
		api.giveInventoryToPlayer(player, Config.joinInventory);
		Logger.debug("<- SIGTutorial::giveTutorialInventory");
	}
}
