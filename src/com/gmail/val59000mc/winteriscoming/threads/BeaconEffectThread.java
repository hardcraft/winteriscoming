package com.gmail.val59000mc.winteriscoming.threads;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import com.gmail.val59000mc.winteriscoming.WIC;
import com.gmail.val59000mc.winteriscoming.effects.EffectsManager;
import com.gmail.val59000mc.winteriscoming.objectives.Objective;
import com.gmail.val59000mc.winteriscoming.objectives.ObjectivesManager;

public class BeaconEffectThread implements Runnable {

	private BeaconEffectThread thread;
	
	public static void start(){
		BeaconEffectThread thread = new BeaconEffectThread();
		Bukkit.getScheduler().runTaskAsynchronously(WIC.getPlugin(), thread);
	}
	
	private BeaconEffectThread() {
		thread = this;
	}

	@Override
	public void run() {
		
		Bukkit.getScheduler().runTask(WIC.getPlugin(), new Runnable() {
			
			@Override
			public void run() {
				
				Bukkit.getScheduler().runTask(WIC.getPlugin(), new Runnable(){

					@Override
					public void run() {
						
						for(Objective objective : ObjectivesManager.instance().getCurrentObjectives()){
							if(!objective.isBroken()){
								Location start = objective.getLocation().clone().add(0.5,0,0.5);
								Location end = start.clone().add(0,90,0);
								EffectsManager.drawLine(start,end);
							}
						}
						
						Bukkit.getScheduler().runTaskLaterAsynchronously(WIC.getPlugin(), thread, 20);
						
					}
					
				});
				
			}
		});
		
	}
	
}
