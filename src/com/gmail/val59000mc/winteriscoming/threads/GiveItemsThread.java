package com.gmail.val59000mc.winteriscoming.threads;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.gmail.val59000mc.winteriscoming.WIC;
import com.gmail.val59000mc.winteriscoming.players.PlayersManager;
import com.gmail.val59000mc.winteriscoming.players.WicPlayer;

public class GiveItemsThread implements Runnable {

	private static GiveItemsThread instance;
	private ItemStack arrow;
	private ItemStack vine;
	private boolean run;
	
	public static void start(){
		instance = new GiveItemsThread();
		instance.run = true;
		Bukkit.getScheduler().runTaskAsynchronously(WIC.getPlugin(), instance);
	}
	
	public static void stop(){
		instance.run = false;
	}
	
	private GiveItemsThread() {
		this.arrow = new ItemStack(Material.ARROW,1);
		this.vine = new ItemStack(Material.VINE,1);
		this.run = false;
	}

	@Override
	public void run() {
		

		if(run){
			
			Bukkit.getScheduler().runTask(WIC.getPlugin(), new Runnable() {
			
			@Override
			public void run() {
				
				for(WicPlayer wicPlayer : PlayersManager.instance().getPlayers()){
					if(wicPlayer.isOnline() && wicPlayer.getTeam() != null){
						switch(wicPlayer.getTeam().getType()){
						case ARMY_OF_THE_DEAD:
							wicPlayer.getPlayer().getInventory().addItem(vine.clone());
							break;
						case NIGHT_WATCH:
							wicPlayer.getPlayer().getInventory().addItem(arrow.clone());
							break;
						default:
							break;
							
						}
					}
				
				}
				
				
				Bukkit.getScheduler().runTaskLaterAsynchronously(WIC.getPlugin(), instance, 200);
				
			}
		});
		
	}
		
	}
	
}
