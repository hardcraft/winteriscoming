package com.gmail.val59000mc.winteriscoming.threads;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;

import com.gmail.val59000mc.spigotutils.Locations;
import com.gmail.val59000mc.winteriscoming.WIC;

public class RemoveAnvilThread implements Runnable {
	
	private static RemoveAnvilThread instance;
	private List<Location> anvilStartLocations;
	private boolean run;

	private RemoveAnvilThread() {
		this.anvilStartLocations = Collections.synchronizedList(new ArrayList<Location>());
	}
	
	public static void start(){
		instance = new RemoveAnvilThread();
		instance.run = true;
		Bukkit.getScheduler().runTaskAsynchronously(WIC.getPlugin(), instance);
	}
	
	public static void stop(){
		instance.run = false;
	}
	
	public static void add(Location location){
		if(instance == null){
			instance = new RemoveAnvilThread();
		}
		instance.getAnvilStartLocation().add(location);
	}
	
	private synchronized List<Location> getAnvilStartLocation(){
		return anvilStartLocations;
	}

	@Override
	public void run() {
		
		if(run){
			Bukkit.getScheduler().runTask(WIC.getPlugin(), new Runnable() {
				
				@Override
				public void run() {
					
					Iterator<Location> iterator = getAnvilStartLocation().iterator();
					while(iterator.hasNext()){
						Location startLocation = iterator.next();
						Location surface = Locations.getSurfaceLocation(startLocation.clone());
						if(surface.getBlock().getType().equals(Material.ANVIL)){
							surface.getBlock().setType(Material.AIR);
							iterator.remove();
						}
					}
					
					Bukkit.getScheduler().runTaskLaterAsynchronously(WIC.getPlugin(), instance, 60);
				}
			});
		}
		
	}

}
