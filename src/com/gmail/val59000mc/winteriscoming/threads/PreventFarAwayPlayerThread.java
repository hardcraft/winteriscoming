package com.gmail.val59000mc.winteriscoming.threads;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.winteriscoming.WIC;
import com.gmail.val59000mc.winteriscoming.configuration.Config;
import com.gmail.val59000mc.winteriscoming.game.GameManager;
import com.gmail.val59000mc.winteriscoming.objectives.LocationBounds;
import com.gmail.val59000mc.winteriscoming.objectives.ObjectivesManager;
import com.gmail.val59000mc.winteriscoming.players.PlayerState;
import com.gmail.val59000mc.winteriscoming.players.PlayersManager;
import com.gmail.val59000mc.winteriscoming.players.WicPlayer;

public class PreventFarAwayPlayerThread implements Runnable {
	
	private static PreventFarAwayPlayerThread thread;
	private boolean run;
	
	public static void start(){
		Logger.debug("-> KillFarAwayPlayerThread::start");
		PreventFarAwayPlayerThread thread = new PreventFarAwayPlayerThread();
		Bukkit.getScheduler().runTaskAsynchronously(WIC.getPlugin(), thread);
		Logger.debug("<- KillFarAwayPlayerThread::start");
	}
	
	private PreventFarAwayPlayerThread() {
		thread = this;
		this.run = true;
	}
	
	public static void stop() {
		thread.run = false;
	}

	@Override
	public void run() {
		
		Bukkit.getScheduler().runTask(WIC.getPlugin(), new Runnable(){

			@Override
			public void run() {
				
				if(run){
					LocationBounds bounds = null;
					LocationBounds specBounds = Config.thirdBounds;
					Location toTp = null;
					
					switch(GameManager.instance().getState()){
						case WAITING:
							bounds = Config.lobbyLocationBounds;
							toTp = Config.lobbyLocation;
							break;
						case PLAYING:
							bounds = ObjectivesManager.instance().getCurrentObjectiveLevel().getBounds();
							break;
						case ENDED:
							bounds = ObjectivesManager.instance().getCurrentObjectiveLevel().getBounds();
							toTp = Config.mapLocation;
						default:
							break;
					}
					
					
					if(bounds != null && bounds.getWorld() != null){

						World world = bounds.getWorld();
						
						for(Player player : Bukkit.getOnlinePlayers()){

							WicPlayer wicPlayer = PlayersManager.instance().getWicPlayer(player);
							
							if(wicPlayer != null && player.getWorld().equals(world) && player.getHealth() > 0){
								
								if(wicPlayer.isState(PlayerState.DEAD)){
									if(!specBounds.contains(player.getLocation())){
										player.teleport(Config.mapLocation);
										wicPlayer.sendI18nMessage("game.leave-arena");
									}
								}else if(!bounds.contains(player.getLocation())){
									if(toTp == null && wicPlayer.getTeam() != null){
										toTp = wicPlayer.getTeam().getSpawnPoint();
										player.teleport(toTp);
										wicPlayer.sendI18nMessage("game.leave-arena");
									}
								}
							}
						}
					}
					
					Bukkit.getScheduler().runTaskLaterAsynchronously(WIC.getPlugin(), thread, 60);
				}
				
				
			}
			
		});
		
	}

}
