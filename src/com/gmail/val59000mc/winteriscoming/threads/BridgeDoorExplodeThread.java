package com.gmail.val59000mc.winteriscoming.threads;

import org.bukkit.Bukkit;
import org.bukkit.Sound;

import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Sounds;
import com.gmail.val59000mc.spigotutils.Time;
import com.gmail.val59000mc.winteriscoming.WIC;
import com.gmail.val59000mc.winteriscoming.configuration.Config;
import com.gmail.val59000mc.winteriscoming.i18n.I18n;

public class BridgeDoorExplodeThread implements Runnable {

	private int remainingTime;
	private int explosions;
	private BridgeDoorExplodeThread thread;
	
	public static void start(){
		BridgeDoorExplodeThread thread = new BridgeDoorExplodeThread();
		Bukkit.getScheduler().runTaskAsynchronously(WIC.getPlugin(), thread);
	}
	
	private BridgeDoorExplodeThread() {
		thread = this;
		this.explosions = 0;
		this.remainingTime = 25;
	}

	@Override
	public void run() {
		
		Bukkit.getScheduler().runTask(WIC.getPlugin(), new Runnable() {
			
			@Override
			public void run() {
				if((remainingTime >0 && remainingTime <= 5) || (remainingTime > 5 && remainingTime%10 == 0)){
					Sounds.playAll(Sound.CLICK, 0.5f, 2);
					Logger.broadcast(I18n.get("bridge.explode-in."+Config.map).replace("%time%",Time.getFormattedTime(remainingTime)));
				}
				
				if(remainingTime<=0){
					if(explosions == 0){
						Bukkit.getWorld(Config.worldName).createExplosion(Config.doorLocation, 8F, false);
					}
					if(explosions < Config.bridgelocations.size()){
						Bukkit.getWorld(Config.worldName).createExplosion(Config.bridgelocations.get(explosions), 8F, false);
						explosions++;
						Bukkit.getScheduler().runTaskLaterAsynchronously(WIC.getPlugin(), thread, 10);
					}
				}else{
					remainingTime--;
					Bukkit.getScheduler().runTaskLaterAsynchronously(WIC.getPlugin(), thread, 20);
				}
			}
		});
		
	}
	
}
