package com.gmail.val59000mc.winteriscoming.threads;

import org.bukkit.Bukkit;
import org.bukkit.Sound;

import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Sounds;
import com.gmail.val59000mc.spigotutils.Time;
import com.gmail.val59000mc.winteriscoming.WIC;
import com.gmail.val59000mc.winteriscoming.configuration.Config;
import com.gmail.val59000mc.winteriscoming.game.EndCause;
import com.gmail.val59000mc.winteriscoming.game.GameManager;
import com.gmail.val59000mc.winteriscoming.i18n.I18n;
import com.gmail.val59000mc.winteriscoming.objectives.ObjectivesManager;

public class RemainingTimeThread implements Runnable{


	private static RemainingTimeThread instance;
	
	private long remainingTime;
	private long elapsedTime;
	private boolean run;
	

	public static void start() {
		Logger.debug("-> TimeBeforeEndThread::start");
		RemainingTimeThread thread = new RemainingTimeThread();
		Bukkit.getScheduler().runTaskAsynchronously(WIC.getPlugin(), thread);
		Logger.debug("<- TimeBeforeEndThread::start");
	}
	
	public static void stop() {
		Logger.debug("-> TimeBeforeEndThread::stop");
		instance.run = false;
		Logger.debug("<- TimeBeforeEndThread::stop");
	}
	
	
	public RemainingTimeThread(){
		instance = this;
		this.remainingTime = Config.attackCountdown;
		this.elapsedTime = 0;
		this.run = true;
	}
	
	@Override
	public void run() {
		
		Bukkit.getScheduler().runTask(WIC.getPlugin(), new Runnable(){

			@Override
			public void run() {
				GameManager gm = GameManager.instance();

				if(run){
					if(remainingTime <= 0){
						gm.endGame(EndCause.DEFENSE_WIN);
					}else{
						if(remainingTime < 10 || (remainingTime < 60 && remainingTime%15 == 0)){
							Sounds.playAll(Sound.CLICK, 1, 2);
							Logger.broadcast(I18n.get("game.remaining-time").replace("%time%", Time.getFormattedTime(remainingTime)));
						}
						
						remainingTime--;
						elapsedTime++;
						
						if(ObjectivesManager.instance().isAllObjectivesBroken()){
							gm.endGame(EndCause.ATTACK_WIN);
						}else{
							Bukkit.getScheduler().runTaskLaterAsynchronously(WIC.getPlugin(), instance, 20);	
						}
							
					}
				}
				
				
				
				
			}
			
		});
		
		
	}
	
	public static long getRemainingTime(){
		return instance.remainingTime;
	}
	
	public static long getElapsedTime(){
		return instance.elapsedTime;
	}
	
	public static void addRemainingTime(long time){
		instance.remainingTime += time;
	}
	
}
