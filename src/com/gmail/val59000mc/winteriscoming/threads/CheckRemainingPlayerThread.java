package com.gmail.val59000mc.winteriscoming.threads;

import org.bukkit.Bukkit;

import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.winteriscoming.WIC;
import com.gmail.val59000mc.winteriscoming.game.EndCause;
import com.gmail.val59000mc.winteriscoming.game.GameManager;
import com.gmail.val59000mc.winteriscoming.players.PlayersManager;
import com.gmail.val59000mc.winteriscoming.players.WicTeam;


public class CheckRemainingPlayerThread implements Runnable{

	private static CheckRemainingPlayerThread instance;
	
	private boolean run;
	
	public static void start(){
		Logger.debug("-> CheckRemainingPlayerThread::start");
		CheckRemainingPlayerThread thread = new CheckRemainingPlayerThread();
		Bukkit.getScheduler().runTaskAsynchronously(WIC.getPlugin(), thread);
		Logger.debug("<- CheckRemainingPlayerThread::start");
	}



	public static void stop() {
		instance.run = false;
	}
	
	
	public CheckRemainingPlayerThread(){
		instance = this;
		this.run = true;
	}
	
	@Override
	public void run() {
		
		Bukkit.getScheduler().runTask(WIC.getPlugin(), new Runnable(){

			@Override
			public void run(){
				// to do
				if(run){
					WicTeam attack = PlayersManager.instance().getAttackTeam();
					WicTeam defense = PlayersManager.instance().getDefenseTeam();
					if(!attack.isOnline()){
						GameManager.instance().endGame(EndCause.DEFENSE_WIN);
					}else if(!defense.isOnline()){
						GameManager.instance().endGame(EndCause.ATTACK_WIN);
					}else{
						Bukkit.getScheduler().runTaskLaterAsynchronously(WIC.getPlugin(), instance, 20);	
					}
				}
			}
			
		});

	}
}
