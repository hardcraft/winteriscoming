package com.gmail.val59000mc.winteriscoming.threads;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.spigotutils.SimpleScoreboard;
import com.gmail.val59000mc.spigotutils.Time;
import com.gmail.val59000mc.winteriscoming.WIC;
import com.gmail.val59000mc.winteriscoming.i18n.I18n;
import com.gmail.val59000mc.winteriscoming.objectives.Objective;
import com.gmail.val59000mc.winteriscoming.objectives.ObjectivesManager;
import com.gmail.val59000mc.winteriscoming.players.WicPlayer;

public class UpdateScoreboardThread implements Runnable{
	

	private static UpdateScoreboardThread instance;
	private Map<WicPlayer,SimpleScoreboard> scoreboards;
	
	
	public static void add(WicPlayer wicPlayer){
		if(instance == null){
			instance = new UpdateScoreboardThread();
		}
		instance.addWicPlayer(wicPlayer);
	}
	
	private void addWicPlayer(WicPlayer wicPlayer){
		SimpleScoreboard sc = new SimpleScoreboard("Winter Is Coming");
		getScoreboards().put(wicPlayer, sc);
	}

	public static void start(){
		if(instance != null){
			Bukkit.getScheduler().runTaskAsynchronously(WIC.getPlugin(), instance);
		}
	}
	
	private UpdateScoreboardThread(){
		this.scoreboards = Collections.synchronizedMap(new HashMap<WicPlayer,SimpleScoreboard>());
	}
	
	private synchronized Map<WicPlayer,SimpleScoreboard> getScoreboards(){
		return scoreboards;
	}
	
	@Override
	public void run() {
		Bukkit.getScheduler().runTask(WIC.getPlugin(), new Runnable(){

			@Override
			public void run() {
				
				Bukkit.getScheduler().runTask(WIC.getPlugin(), new Runnable(){

					@Override
					public void run() {
						
						for(Entry<WicPlayer,SimpleScoreboard> entry : getScoreboards().entrySet()){
							WicPlayer wicPlayer = entry.getKey();
							SimpleScoreboard scoreboard = entry.getValue();
							
							
							if(wicPlayer.isOnline() && wicPlayer.getTeam() != null && wicPlayer.isPlaying()){
								updatePlayingScoreboard(wicPlayer,scoreboard);
							}else if(wicPlayer.isOnline()){
								updateSpectatingScoreboard(wicPlayer, scoreboard);
							}
							
							
							if(wicPlayer.isOnline() && wicPlayer.getTeam() != null){

								
							}
						}

						Bukkit.getScheduler().runTaskLaterAsynchronously(WIC.getPlugin(), instance, 20);
					}

					private void updateSpectatingScoreboard(WicPlayer wicPlayer, SimpleScoreboard scoreboard) {
						Player player = wicPlayer.getPlayer();
						
						List<String> content = new ArrayList<String>();
						content.add(" ");
						
						// Objectives
						content.add(I18n.get("scoreboard.objectives",player));
						String objectivesString = ChatColor.GREEN+I18n.get(ObjectivesManager.instance().getCurrentObjectiveLevel().getCode());
						for(Objective objective : ObjectivesManager.instance().getCurrentObjectives()){
							if(objective.isBroken()){
								objectivesString += " "+ChatColor.RED+"\u25A0";
							}else{
								objectivesString += " "+ChatColor.GREEN+"\u25A0";
							}
						}
						content.add(" "+objectivesString);
						
						// Time left
						content.add(I18n.get("scoreboard.time-left",player));
						content.add(" "+ChatColor.GREEN+Time.getFormattedTime(RemainingTimeThread.getRemainingTime()));
				        

						scoreboard.clear();
						for(String line : content){
							scoreboard.add(line);
						}
						scoreboard.draw();
						scoreboard.send(player);
						
					}

					private void updatePlayingScoreboard(WicPlayer wicPlayer, SimpleScoreboard scoreboard) {
						
						Player player = wicPlayer.getPlayer();
						
						List<String> content = new ArrayList<String>();
						content.add(" ");
						
						// Team
						content.add(I18n.get("scoreboard.team",player));
						content.add(" "+ChatColor.GREEN+I18n.get(wicPlayer.getTeam().getType().getCode(),player));
						
						if(wicPlayer.getPlayerClass() != null){
							// Player class
							content.add(I18n.get("scoreboard.player-class",player));
							content.add(" "+ChatColor.GREEN+I18n.get(wicPlayer.getPlayerClass().getType().getCode(),player));
						}
						
						// Kills / Deaths
						content.add(I18n.get("scoreboard.kills-deaths",player));
						content.add(" "+ChatColor.GREEN+""+wicPlayer.getKills()+ChatColor.WHITE+" / "+ChatColor.GREEN+wicPlayer.getDeaths());
						
						// Coins
						content.add(I18n.get("scoreboard.coins-earned",player));
						content.add(" "+ChatColor.GREEN+""+wicPlayer.getMoney());
						
						// Objectives
						content.add(I18n.get("scoreboard.objectives",player));
						String objectivesString = ChatColor.GREEN+I18n.get(ObjectivesManager.instance().getCurrentObjectiveLevel().getCode());
						for(Objective objective : ObjectivesManager.instance().getCurrentObjectives()){
							if(objective.isBroken()){
								objectivesString += " "+ChatColor.RED+"\u25A0";
							}else{
								objectivesString += " "+ChatColor.GREEN+"\u25A0";
							}
						}
						content.add(" "+objectivesString);
						
						// Time left
						content.add(I18n.get("scoreboard.time-left",player));
						content.add(" "+ChatColor.GREEN+Time.getFormattedTime(RemainingTimeThread.getRemainingTime()));
				        

						scoreboard.clear();
						for(String line : content){
							scoreboard.add(line);
						}
						scoreboard.draw();
						scoreboard.send(player);
						
					}
					
				});
				
				
				
				
			}});
		
	}
	
	
	
	
}
