package com.gmail.val59000mc.winteriscoming.objectives;

import com.gmail.val59000mc.winteriscoming.configuration.Config;

public enum ObjectiveLevel {
	ONE("objectives.fence."+Config.map),
	TWO("objectives.forest."+Config.map),
	THREE("objectives.wall."+Config.map);
	
	private String code;
	private LocationBounds bounds;
	
	private ObjectiveLevel(String code){
		this.code = code;
	}
	
	public String getCode(){
		return code;
	}

	public LocationBounds getBounds() {
		return bounds;
	}

	public void setBounds(LocationBounds bounds) {
		this.bounds = bounds;
	}
	
}
