package com.gmail.val59000mc.winteriscoming.objectives;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;

import com.gmail.val59000mc.spigotutils.Fireworks;
import com.gmail.val59000mc.spigotutils.Locations;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Parser;
import com.gmail.val59000mc.spigotutils.Sounds;
import com.gmail.val59000mc.winteriscoming.WIC;
import com.gmail.val59000mc.winteriscoming.configuration.Config;
import com.gmail.val59000mc.winteriscoming.i18n.I18n;
import com.gmail.val59000mc.winteriscoming.players.PlayersManager;
import com.gmail.val59000mc.winteriscoming.players.TeamType;
import com.gmail.val59000mc.winteriscoming.players.WicPlayer;
import com.gmail.val59000mc.winteriscoming.threads.RemainingTimeThread;

public class ObjectivesManager {
	
	private static ObjectivesManager instance;
	
	private Map<ObjectiveLevel,List<Objective>> objectives;
	
	public static ObjectivesManager instance(){
		if(instance == null){
			instance = new ObjectivesManager();
		}
		
		return instance;
	}
	
	public synchronized Map<ObjectiveLevel,List<Objective>> getObjectives(){
		return objectives;
	}
	
	private ObjectivesManager(){
		objectives = Collections.synchronizedMap(new HashMap<ObjectiveLevel,List<Objective>>());
	}
	
	public static void load(){
		Logger.debug("-> ObjectivesManager::load");
		FileConfiguration cfg = WIC.getPlugin().getConfig();
		World world = Bukkit.getWorld(Config.worldName);
		

		// load objective bounds
		Location bound1 = Parser.parseLocation(world, cfg.getString("objectives.bound1"));
		Location bound2 = Parser.parseLocation(world, cfg.getString("objectives.bound2"));
		Location bound3 = Parser.parseLocation(world, cfg.getString("objectives.bound3"));
		Location bound4 = Parser.parseLocation(world, cfg.getString("objectives.bound4"));
		ObjectiveLevel.ONE.setBounds(new LocationBounds(bound1, bound2));
		ObjectiveLevel.TWO.setBounds(new LocationBounds(bound1, bound3));
		ObjectiveLevel.THREE.setBounds(new LocationBounds(bound1, bound4));
		
		
		for(ObjectiveLevel level : ObjectiveLevel.values()){
			List<String> objectivesLocationStr = cfg.getStringList("objectives."+level);
			if(objectivesLocationStr == null || objectivesLocationStr.size() != 3){
				Logger.severeC(I18n.get("objectives.not-found"));
			}else{
				
				List<Objective> objectivesList = new ArrayList<Objective>();
				
				for(String locationStr : objectivesLocationStr){
					Location location = Parser.parseLocation(world,locationStr);
					Logger.debug("+ objective "+level+" at "+Locations.printLocation(location));
					objectivesList.add(new Objective(location,level));
				}
				
				instance().getObjectives().put(level, objectivesList);
			}
		}
		Logger.debug("<- ObjectivesManager::load");
	}
	
	public List<Objective> getCurrentObjectives(){
		for(ObjectiveLevel level : ObjectiveLevel.values()){
			for(Objective objective : getObjectives().get(level)){
				if(!objective.isBroken()){
					return getObjectives().get(objective.getLevel());
				}
			}
		}
		
		return getObjectives().get(ObjectiveLevel.THREE);
	}
	
	public ObjectiveLevel getCurrentObjectiveLevel(){
		return getCurrentObjectives().get(0).getLevel();
	}
	
	public Objective getObjective(Block block){
		if(block == null || !block.getType().equals(Material.BEACON)){
			return null;
		}
		
		for(Entry<ObjectiveLevel,List<Objective>> entry : getObjectives().entrySet()){
			for(Objective objective : entry.getValue()){
				if(objective.is(block)){
					return objective;
				}
			}
		}
		return null;
	}
	
	
	public boolean tryBreakObjective(Objective objective, WicPlayer wicPlayer){
		Logger.debug("-> ObjectivesManager:: canBreakObjective, objective="+objective+", wicPlayer="+wicPlayer);
		ObjectiveLevel oldLevel = getCurrentObjectiveLevel();
		
		if(objective == null || wicPlayer == null || !wicPlayer.isOnline() || (objective != null && objective.isBroken())){
			Logger.debug("<- ObjectivesManager:: canBreakObjective, canBreak=false");
			return false;
		}
		
		if(wicPlayer.getTeam().getType().equals(TeamType.NIGHT_WATCH)){
			wicPlayer.sendI18nMessage("objectives.cannot-break-team-grief");
			Logger.debug("<- ObjectivesManager:: canBreakObjective, canBreak=false");
			return false;
		}
		
		if(objective.getLevel().equals(oldLevel)){
			
			// break objective
			doBreakObjective(objective, wicPlayer);
			
			// Check if defense line broken
			if(!oldLevel.equals(getCurrentObjectiveLevel())){
				PlayersManager.instance().moveToNextDefenseLine();
			}
			
			Logger.debug("<- ObjectivesManager:: canBreakObjective, canBreak=true");
			return true;
		}else{
			wicPlayer.sendI18nMessage("objectives.cannot-break-other-level");
			Logger.debug("<- ObjectivesManager:: canBreakObjective, canBreak=false");
			return false;
		}
	}

	public boolean isAllObjectivesBroken(){
		if(getCurrentObjectiveLevel().equals(ObjectiveLevel.THREE)){
			for(Objective objective : getCurrentObjectives()){
				if(!objective.isBroken()){
					return false;
				}
			}
			
			return true;
		}
		return false;
	}
	
	private void doBreakObjective(Objective objective, WicPlayer wicPlayer){

		Logger.broadcast(I18n.get("objectives.broken-by").replace("%player%", wicPlayer.getName()));
		Sounds.playAll(Sound.ZOMBIE_WOODBREAK, 1.5f, 0.5f);
		Fireworks.spawnRandomFirework(objective.getLocation());
		objective.setBroken();
		RemainingTimeThread.addRemainingTime(Config.breakObjectiveTime);
		
	}
}
