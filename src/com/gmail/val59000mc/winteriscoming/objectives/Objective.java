package com.gmail.val59000mc.winteriscoming.objectives;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;

import com.gmail.val59000mc.spigotutils.Locations;

public class Objective {
	private Location location;
	private boolean isBroken;
	private ObjectiveLevel level;
	
	public Objective(Location location, ObjectiveLevel level){
		this.location = location;
		this.level = level;
		this.isBroken = false;
	}
	
	public boolean is(Block block){
		return block.getType().equals(Material.BEACON) && block.getLocation().distanceSquared(location) < 1;
	}
	
	public boolean isBroken(){
		return isBroken;
	}
	
	public void setBroken(){
		this.isBroken = true;
	}

	public ObjectiveLevel getLevel() {
		return level;
	}
	
	public String toString(){
		return "[location='"+Locations.printLocation(location)+"',level='"+level+"', isTaken='"+isBroken+"']";
	}

	public Location getLocation() {
		return location;
	}
	
	
}
