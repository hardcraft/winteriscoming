package com.gmail.val59000mc.winteriscoming.objectives;

import org.bukkit.Location;
import org.bukkit.World;

public class LocationBounds {
	private Location min;
	private Location max;
	public LocationBounds(Location min, Location max) {
		super();
		
		Location min_ = min.clone();
		Location max_ = max.clone();
		
		double minX = Math.min(min_.getX(), max_.getX());
		double minY = Math.min(min_.getY(), max_.getY());
		double minZ = Math.min(min_.getZ(), max_.getZ());
		
		double maxX = Math.max(min_.getX(), max_.getX());
		double maxY = Math.max(min_.getY(), max_.getY());
		double maxZ = Math.max(min_.getZ(), max_.getZ());
		
		World world = min.getWorld();
		
		this.min = new Location(world,minX,minY,minZ);
		this.max = new Location(world,maxX,maxY,maxZ);
	}
	
	public Location getMin() {
		return min;
	}
	public Location getMax() {
		return max;
	}
	public boolean contains(Location location){
		if(min == null || max == null)
			return true;
		
		return location.getX() >= min.getX() && location.getX() <= max.getX()
			&& location.getY() >= min.getY() && location.getY() <= max.getY()
			&& location.getZ() >= min.getZ() && location.getZ() <= max.getZ();
	}

	public World getWorld() {
		if(min != null)
			return min.getWorld();
		return null;
	}
}
