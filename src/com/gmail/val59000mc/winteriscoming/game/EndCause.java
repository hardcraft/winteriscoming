package com.gmail.val59000mc.winteriscoming.game;

import com.gmail.val59000mc.winteriscoming.configuration.Config;

public enum EndCause {
	ATTACK_WIN("game.end.attack-win."+Config.map),
	DEFENSE_WIN("game.end.defense-win."+Config.map),
	NO_MORE_PLAYERS("game.end.no-more-players");
	
	private String code;

	private EndCause(String code) {
		this.code = code;
	}

	public String getCode() {
		return code;
	}
	
	
	
}
