package com.gmail.val59000mc.winteriscoming.game;

public enum GameState {
	LOADING,
	WAITING,
	STARTING,
	PLAYING,
	ENDED;
}
