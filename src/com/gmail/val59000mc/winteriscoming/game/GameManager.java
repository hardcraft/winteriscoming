package com.gmail.val59000mc.winteriscoming.game;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;

import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.winteriscoming.WIC;
import com.gmail.val59000mc.winteriscoming.classes.PlayerClassManager;
import com.gmail.val59000mc.winteriscoming.commands.ChatCommand;
import com.gmail.val59000mc.winteriscoming.commands.StartCommand;
import com.gmail.val59000mc.winteriscoming.configuration.Config;
import com.gmail.val59000mc.winteriscoming.i18n.I18n;
import com.gmail.val59000mc.winteriscoming.listeners.BlockListener;
import com.gmail.val59000mc.winteriscoming.listeners.IceListener;
import com.gmail.val59000mc.winteriscoming.listeners.PingListener;
import com.gmail.val59000mc.winteriscoming.listeners.PlayerChatListener;
import com.gmail.val59000mc.winteriscoming.listeners.PlayerConnectionListener;
import com.gmail.val59000mc.winteriscoming.listeners.PlayerDamageListener;
import com.gmail.val59000mc.winteriscoming.listeners.PlayerDeathListener;
import com.gmail.val59000mc.winteriscoming.listeners.PlayerFoodListener;
import com.gmail.val59000mc.winteriscoming.listeners.PlayerInteractListener;
import com.gmail.val59000mc.winteriscoming.maploader.MapLoader;
import com.gmail.val59000mc.winteriscoming.objectives.ObjectivesManager;
import com.gmail.val59000mc.winteriscoming.players.PlayersManager;
import com.gmail.val59000mc.winteriscoming.threads.BeaconEffectThread;
import com.gmail.val59000mc.winteriscoming.threads.CheckRemainingPlayerThread;
import com.gmail.val59000mc.winteriscoming.threads.GiveItemsThread;
import com.gmail.val59000mc.winteriscoming.threads.PreventFarAwayPlayerThread;
import com.gmail.val59000mc.winteriscoming.threads.RemainingTimeThread;
import com.gmail.val59000mc.winteriscoming.threads.RemoveAnvilThread;
import com.gmail.val59000mc.winteriscoming.threads.RestartThread;
import com.gmail.val59000mc.winteriscoming.threads.WaitForNewPlayersThread;
import com.gmail.val59000mc.winteriscoming.tutorial.SIGTutorial;


public class GameManager {

	private static GameManager instance = null;
	
	private GameState state;
	private boolean pvp;
	
	// static
	
	public static GameManager instance(){
		if(instance == null){
			instance = new GameManager();
		}
		return instance;
	}

	
	// constructor 
	
	private GameManager(){
		this.state = GameState.LOADING;
		this.pvp = false;
	}

	
	// accessors
	
	public GameState getState() {
		return state;
	}

	public boolean isState(GameState state) {
		return getState().equals(state);
	}

	public boolean isPvp() {
		return pvp;
	}
	public void setPvp(boolean state) {
		pvp = state;
	}
	
	// methods 
	
	public void loadGame() {
		Logger.debug("-> GameManager::loadNewGame");
		state = GameState.LOADING;
		MapLoader.deleteOldPlayersFiles();
		Config.load();
		PlayerClassManager.load();
		
		MapLoader.load();
		ObjectivesManager.load();
		
		registerListeners();
		registerCommands();
		
		if(Config.isBungeeEnabled)
			WIC.getPlugin().getServer().getMessenger().registerOutgoingPluginChannel(WIC.getPlugin(), "BungeeCord");
		
		if(Config.isSIGLoaded){
			SIGTutorial.load();
		}

		waitForNewPlayers();
		Logger.debug("<- GameManager::loadNewGame");
	}


	
	private void registerListeners(){
		Logger.debug("-> GameManager::registerListeners");
		// Registers Listeners
		List<Listener> listeners = new ArrayList<Listener>();		
		listeners.add(new PlayerConnectionListener());	
		listeners.add(new PlayerChatListener());
		listeners.add(new PlayerDamageListener());
		listeners.add(new PlayerFoodListener());
		listeners.add(new PlayerInteractListener());
		listeners.add(new PlayerDeathListener());
		listeners.add(new IceListener());
		listeners.add(new PingListener());
		listeners.add(new BlockListener());
		for(Listener listener : listeners){
			Logger.debug("Registering listener="+listener.getClass().getSimpleName());
			Bukkit.getServer().getPluginManager().registerEvents(listener, WIC.getPlugin());
		}
		Logger.debug("<- GameManager::registerListeners");
	}
	
	private void registerCommands(){
		Logger.debug("-> GameManager::registerCommands");
		// Registers Listeners	
		WIC.getPlugin().getCommand("chat").setExecutor(new ChatCommand());
		WIC.getPlugin().getCommand("start").setExecutor(new StartCommand());
		Logger.debug("<- GameManager::registerCommands");
	}



	private void waitForNewPlayers(){
		Logger.debug("-> GameManager::waitForNewPlayers");
		WaitForNewPlayersThread.start();
		PreventFarAwayPlayerThread.start();
		state = GameState.WAITING;
		Logger.infoC(I18n.get("game.player-allowed-to-join"));
		Logger.debug("<- GameManager::waitForNewPlayers");
	}
	
	public void startGame(){
		Logger.debug("-> GameManager::startGame");
		state = GameState.STARTING;
		Logger.broadcast(I18n.get("game.start"));
		RemainingTimeThread.start();
		PlayersManager.instance().startAllPlayers();	
		playGame();
		Logger.debug("<- GameManager::startGame");
	}
	
	private void playGame(){
		state = GameState.PLAYING;
		pvp = true;
		GiveItemsThread.start();
		RemoveAnvilThread.start();
		CheckRemainingPlayerThread.start();
		if(Config.isEffectLibLoaded){
			BeaconEffectThread.start();
		}
	}
	
	public void endGame(EndCause cause) {
		if(state.equals(GameState.PLAYING)){
			state = GameState.ENDED;
			pvp = false;
			CheckRemainingPlayerThread.stop();
			PreventFarAwayPlayerThread.stop();
			GiveItemsThread.stop();
			RemoveAnvilThread.stop();
			PlayersManager.instance().endAllPlayers(cause);
			RestartThread.start();
		}
		
	}

	
	


	
	
	

}
