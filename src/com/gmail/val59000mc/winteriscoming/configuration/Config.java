package com.gmail.val59000mc.winteriscoming.configuration;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;

import com.connorlinfoot.bountifulapi.BountifulAPI;
import com.gmail.val59000mc.simpleinventorygui.SIG;
import com.gmail.val59000mc.simpleinventorygui.api.SIGApi;
import com.gmail.val59000mc.simpleinventorygui.exceptions.InventoryParseException;
import com.gmail.val59000mc.simpleinventorygui.exceptions.TriggerParseException;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Parser;
import com.gmail.val59000mc.winteriscoming.WIC;
import com.gmail.val59000mc.winteriscoming.dependencies.SelectTeamActionParser;
import com.gmail.val59000mc.winteriscoming.dependencies.TeamPlaceholder;
import com.gmail.val59000mc.winteriscoming.dependencies.VaultManager;
import com.gmail.val59000mc.winteriscoming.i18n.I18n;
import com.gmail.val59000mc.winteriscoming.objectives.LocationBounds;
import com.gmail.val59000mc.winteriscoming.players.TeamType;

import de.slikey.effectlib.EffectLib;
import net.milkbowl.vault.Vault;

public class Config {
	
	// Dependencies
	public static boolean isVaultLoaded;
	public static boolean isEffectLibLoaded;
	public static boolean isSIGLoaded;
	public static boolean isBountifulApiLoaded;
	
	// World 
	public static String lastWorldName;
	public static String worldName;
	public static boolean isLoadLastWord;

	// Map (game-of-thrones or lord-of-the-rings)
	public static String map;
	
	// Locations
	public static String lobbyLocationStr;
	public static String mapLocationStr;
	public static List<String> bridgelocationsStr;
	public static String doorLocationStr;
	public static String bound1Str;
	public static String bound2Str;
	public static String bound3Str;
	public static String bound4Str;
	
	public static Location lobbyLocation;
	public static LocationBounds lobbyLocationBounds;
	public static LocationBounds firstBounds;
	public static LocationBounds secondBounds;
	public static LocationBounds thirdBounds;
	public static Location mapLocation;
	public static List<Location> bridgelocations;
	public static Location doorLocation;
	
	// Players
	public static int minPlayers;
	public static int startCountDown;
	public static int maxPlayers;
	public static int killAfterDisconnect;
	
	// Time
	public static int attackCountdown;
	public static int breakObjectiveTime;
	public static int breakDefenseLineTime;
	
	// Teams
	public static String spawn1AtttackStr;
	public static String spawn2AtttackStr;
	public static String spawn3AtttackStr;
	public static String spawn1DefenseStr;
	public static String spawn2DefenseStr;
	public static String spawn3DefenseStr;
	public static Location spawn1Attack;
	public static Location spawn2Attack;
	public static Location spawn3Attack;
	public static Location spawn1Defense;
	public static Location spawn2Defense;
	public static Location spawn3Defense;
	
	// Join Inventory
	public static String joinInventory;
	
	// Bungee
	public static boolean isBungeeEnabled;
	public static String bungeeServer;
	
	// Rewards
	public static double killReward;
	public static double winReward;
	public static double objectiveReward;
	public static double vipRewardBonus;
	public static double vipPlusRewardBonus;
	public static double helperRewardBonus;
	public static double builderRewardBonus;
	public static double moderateurRewardBonus;
	public static double administrateurRewardBonus;
	
	public static void load(){
		
		FileConfiguration cfg = WIC.getPlugin().getConfig();

		// Debug
		Logger.setDebug(cfg.getBoolean("debug",false));
		
		Logger.debug("-> Config::load");
		
		// World
		lastWorldName = cfg.getString("world.last-world","last_wic_world");
		isLoadLastWord = cfg.getBoolean("world.load-last-world",true);
		worldName = UUID.randomUUID().toString();
		
		// Map
		map = cfg.getString("map","game-of-thrones");
		Logger.debug("Map type : "+map);
		
		// Locations
		lobbyLocationStr = cfg.getString("locations.lobby","334 33 -292 0 0");
		mapLocationStr = cfg.getString("locations.map","0 60 0 0 0");
		bridgelocationsStr = cfg.getStringList("locations.bridge");
		doorLocationStr = cfg.getString("locations.door","338 18 -215 0 0");
		bound1Str = cfg.getString("objectives.bound1","175 130 -144 0 0");
		bound2Str = cfg.getString("objectives.bound2","362 -1000 -287 0 0");
		bound3Str = cfg.getString("objectives.bound3","480 -1000 -290 0 0");
		bound4Str = cfg.getString("objectives.bound4","592 -1000 -295 0 0");
		
		
		// Players
		minPlayers = cfg.getInt("players.min",2);
		maxPlayers = cfg.getInt("players.max",15);
		startCountDown = cfg.getInt("players.start-countdown",15);
		killAfterDisconnect = cfg.getInt("players.kill-after-disconnect",300);
		
		// Time
		attackCountdown = cfg.getInt("time.attack-countdown",300);
		breakObjectiveTime = cfg.getInt("time.break-objective",30);
		breakDefenseLineTime = cfg.getInt("time.break-defense-line",180);
		
		// Teams
		spawn1AtttackStr = cfg.getString("teams.ARMY_OF_THE_DEAD.spawn1", "230 26 -213 -90 0");
		spawn2AtttackStr = cfg.getString("teams.ARMY_OF_THE_DEAD.spawn2", "352 16 -215 -90 0");
		spawn3AtttackStr = cfg.getString("teams.ARMY_OF_THE_DEAD.spawn3", "429 15 -215 -90 0");
		spawn1DefenseStr = cfg.getString("teams.NIGHT_WATCH.spawn1", "352 16 -215 90 0");
		spawn2DefenseStr = cfg.getString("teams.NIGHT_WATCH.spawn2", "429 15 -215 90 0");
		spawn3DefenseStr = cfg.getString("teams.NIGHT_WATCH.spawn3", "519 43 -217 90 0");
				
		// Tutorial
		joinInventory = cfg.getString("join-inventory","join");
		
		// Bungee
		isBungeeEnabled = cfg.getBoolean("bungee.enable",false);
		bungeeServer = cfg.getString("bungee.server","lobby");
		
		// Rewards
		killReward = cfg.getDouble("reward.kill",5);
		winReward = cfg.getDouble("reward.win",50);
		objectiveReward = cfg.getDouble("reward.objective",15);
		vipRewardBonus = cfg.getDouble("reward.vip",100);
		vipPlusRewardBonus = cfg.getDouble("reward.vip+",100);
		helperRewardBonus = cfg.getDouble("reward.helper",100);
		builderRewardBonus = cfg.getDouble("reward.builder",100);
		moderateurRewardBonus = cfg.getDouble("reward.moderateur",100);
		administrateurRewardBonus = cfg.getDouble("reward.administrateur",100);
		
		// Dependencies
		loadVault();
		loadEffectLib();
		loadSIG();
		loadBountifulApi();
		
		// Sig
		if(isSIGLoaded){
			
			SIGApi sig = SIG.getPlugin().getAPI();
			sig.registerActionParser(new SelectTeamActionParser());
			sig.registerPlaceholder(new TeamPlaceholder("{members.ARMY_OF_THE_DEAD}",TeamType.ARMY_OF_THE_DEAD));
			sig.registerPlaceholder(new TeamPlaceholder("{members.NIGHT_WATCH}",TeamType.NIGHT_WATCH));
			
			try {
				sig.registerConfigurationFile(new File(WIC.getPlugin().getDataFolder(), "teams.yml"));
			} catch (InventoryParseException | TriggerParseException e) {
				//
			}
			
		}
		
		
		VaultManager.setupEconomy();
		Logger.debug("<- Config::load");
	}
	
	private static void loadBountifulApi(){
		Logger.debug("-> Config::loadBountifulApi");
		
		Plugin bountifulApi = Bukkit.getPluginManager().getPlugin("BountifulAPI");
        if(bountifulApi == null || !(bountifulApi instanceof BountifulAPI)) {
            Logger.warn(I18n.get("config.dependency.bountiful-not-found"));
        	 isBountifulApiLoaded = false;
        }else{
            Logger.warn(I18n.get("config.dependency.bountifulapi-loaded"));
            isBountifulApiLoaded = true;
        }
		Logger.debug("<- Config::loadBountifulApi");
	}
	
	private static void loadVault(){
		Logger.debug("-> Config::loadVault");
		
		Plugin vault = Bukkit.getPluginManager().getPlugin("Vault");
        if(vault == null || !(vault instanceof Vault)) {
            Logger.warn(I18n.get("config.dependency.vault-not-found"));
        	 isVaultLoaded = false;
        }else{
            Logger.warn(I18n.get("config.dependency.vault-loaded"));
        	 isVaultLoaded = true;
        }
		Logger.debug("<- Config::loadVault");
	}
	
	private static void loadEffectLib(){
		Logger.debug("-> Config::loadEffectLib");
		
		Plugin effecftLib = Bukkit.getPluginManager().getPlugin("EffectLib");
        if(effecftLib == null || !(effecftLib instanceof EffectLib)) {
            Logger.warn(I18n.get("config.dependency.effectlib-not-found"));
        	 isEffectLibLoaded = false;
        }else{
            Logger.warn(I18n.get("config.dependency.effectlib-loaded"));
            isEffectLibLoaded = true;
        }
		Logger.debug("<- Config::loadEffectLib");
	}
	
	private static void loadSIG(){
		Logger.debug("-> Config::loadSIG");
		
		Plugin sig = Bukkit.getPluginManager().getPlugin("SimpleInventoryGUI");
        if(sig == null || !(sig instanceof SIG)) {
            Logger.warn(I18n.get("config.dependency.sig-not-found"));
        	 isSIGLoaded = false;
        }else{
            Logger.warn(I18n.get("config.dependency.sig-loaded"));
            isSIGLoaded = true;
        }
		Logger.debug("<- Config::loadSIG");
	}



	public static void loadWorldLocations() {
		Logger.debug("-> Config::loadWorldLocations");
		
		World world = Bukkit.getWorld(worldName);
		
		lobbyLocation = Parser.parseLocation(world,  lobbyLocationStr);
		lobbyLocationBounds = new LocationBounds(lobbyLocation.clone().add(20,20,20), lobbyLocation.clone().add(-20,-20,-20));
		firstBounds = new LocationBounds(Parser.parseLocation(world,bound1Str), Parser.parseLocation(world,bound2Str));
		secondBounds = new LocationBounds(Parser.parseLocation(world,bound1Str), Parser.parseLocation(world,bound3Str));
		thirdBounds = new LocationBounds(Parser.parseLocation(world,bound1Str), Parser.parseLocation(world,bound4Str));
		
		mapLocation = Parser.parseLocation(world,mapLocationStr);
		
		spawn1Attack = Parser.parseLocation(world, spawn1AtttackStr);
		spawn2Attack = Parser.parseLocation(world, spawn2AtttackStr);
		spawn3Attack = Parser.parseLocation(world, spawn3AtttackStr);
		spawn1Defense = Parser.parseLocation(world, spawn1DefenseStr);
		spawn2Defense = Parser.parseLocation(world, spawn2DefenseStr);
		spawn3Defense = Parser.parseLocation(world, spawn3DefenseStr);
		
		bridgelocations = new ArrayList<Location>();
		for(String bridgeStr : bridgelocationsStr){
			bridgelocations.add(Parser.parseLocation(world, bridgeStr));
		}
		
		doorLocation = Parser.parseLocation(world,doorLocationStr);
		
		
		Logger.debug("<- Config::loadWorldLocations");
		
	}
		
		
		
		
}
