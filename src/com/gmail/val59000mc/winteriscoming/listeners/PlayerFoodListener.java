package com.gmail.val59000mc.winteriscoming.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.potion.PotionEffectType;

import com.gmail.val59000mc.spigotutils.Effects;
import com.gmail.val59000mc.winteriscoming.WIC;
import com.gmail.val59000mc.winteriscoming.players.PlayersManager;
import com.gmail.val59000mc.winteriscoming.players.TeamType;
import com.gmail.val59000mc.winteriscoming.players.WicPlayer;

public class PlayerFoodListener implements Listener {
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerEat(FoodLevelChangeEvent event) {
		
		if(event.getEntity() instanceof Player){

			WicPlayer wicPlayer = PlayersManager.instance().getWicPlayer((Player) event.getEntity());
			
			if(wicPlayer == null || !wicPlayer.isOnline()){
				return;
			}
			
			addSaturation(event,wicPlayer);
			handleEatRottenFlesh(event,wicPlayer);
		}
		
	}
	
	public void addSaturation(FoodLevelChangeEvent event, final WicPlayer wicPlayer){
		if(wicPlayer.isOnline()){
			Float saturation = wicPlayer.getPlayer().getSaturation();
			wicPlayer.getPlayer().setSaturation(saturation+5);
		}
	}
	
	public void handleEatRottenFlesh(FoodLevelChangeEvent event, final WicPlayer wicPlayer){
		if(wicPlayer.isTeam(TeamType.ARMY_OF_THE_DEAD) && wicPlayer.isOnline()){
			Bukkit.getScheduler().runTaskLater(WIC.getPlugin(), new Runnable() {
				
				@Override
				public void run() {
					Effects.remove(wicPlayer.getPlayer(),PotionEffectType.HUNGER);
				}
			}, 1);
		}
	}
	
}
