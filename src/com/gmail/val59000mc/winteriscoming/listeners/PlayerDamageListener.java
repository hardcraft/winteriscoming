package com.gmail.val59000mc.winteriscoming.listeners;

import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.gmail.val59000mc.spigotutils.Effects;
import com.gmail.val59000mc.spigotutils.Sounds;
import com.gmail.val59000mc.winteriscoming.classes.PlayerClassType;
import com.gmail.val59000mc.winteriscoming.game.GameManager;
import com.gmail.val59000mc.winteriscoming.game.GameState;
import com.gmail.val59000mc.winteriscoming.players.PlayerState;
import com.gmail.val59000mc.winteriscoming.players.PlayersManager;
import com.gmail.val59000mc.winteriscoming.players.WicPlayer;

public class PlayerDamageListener implements Listener{
	
	private Map<String,Long> lastWhiteWalkerFreeze;
	private Map<String,Long> lastMestreOneShot;
	private Map<String,Long> lastZombiePoison;
	private Map<String,Long> lastSoldierDamage;
	
	public PlayerDamageListener(){
		this.lastMestreOneShot = Collections.synchronizedMap(new HashMap<String,Long>());
		this.lastWhiteWalkerFreeze = Collections.synchronizedMap(new HashMap<String,Long>());
		this.lastZombiePoison = Collections.synchronizedMap(new HashMap<String,Long>());
		this.lastSoldierDamage = Collections.synchronizedMap(new HashMap<String,Long>());
	}
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerDamage(EntityDamageByEntityEvent event){
		handlePvpAndFriendlyFire(event);
		handleArrow(event);
	}

	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerDamage(EntityDamageEvent event){
		handleAnyDamage(event);
	}
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPotionSplash(PotionSplashEvent event){
		handlePotionSplash(event);
	}
	
	
	///////////////////////
	// PotionSplashEvent //
	///////////////////////
	
	private void handlePotionSplash(PotionSplashEvent event) {
		if(event.getEntity().getShooter() instanceof Player){
			PlayersManager pm = PlayersManager.instance();
			
			Player damager = (Player) event.getEntity().getShooter();
			WicPlayer tDamager = pm.getWicPlayer(damager);
			
			if(tDamager != null){
			
				if(isAttackPotion(event.getPotion())){
					// Cancelling potion damage for teamates
					for(LivingEntity living : event.getAffectedEntities()){
						if(living instanceof Player){
							Player damaged = (Player) living;
							WicPlayer tDamaged = pm.getWicPlayer(damaged);
							if(tDamager.isInTeamWith(tDamaged)){
								event.setIntensity(living, 0);
							}
						}
					}
				}
				
			}
		}
	
	}
	
	// Only checking the first potion effect, considering vanilla potions
	private boolean isAttackPotion(ThrownPotion potion){
		Collection<PotionEffect> effects = potion.getEffects();
		if(effects.size() > 0){
			PotionEffectType effect = effects.iterator().next().getType();
			return ( 
				effect.equals(PotionEffectType.HARM) ||
			    effect.equals(PotionEffectType.POISON) ||
			    effect.equals(PotionEffectType.WEAKNESS) ||
				effect.equals(PotionEffectType.SLOW) ||
			    effect.equals(PotionEffectType.SLOW_DIGGING) ||
			    effect.equals(PotionEffectType.CONFUSION) ||
			    effect.equals(PotionEffectType.BLINDNESS) ||
			    effect.equals(PotionEffectType.HUNGER) ||
			    effect.equals(PotionEffectType.WITHER)
			  );
		}
		return false;
	}
	
	
	///////////////////////
	// EntityDamageEvent //
	///////////////////////
	
	private void handleAnyDamage(EntityDamageEvent event){
		
		if(event.getEntity() instanceof Player){
			
			if(!GameManager.instance().isState(GameState.PLAYING)){
				event.setCancelled(true);
			}
		
		}
	}
	
	///////////////////////////////
	// EntityDamageByEntityEvent //
	///////////////////////////////
	
	private void handlePvpAndFriendlyFire(EntityDamageByEntityEvent event){

		PlayersManager pm = PlayersManager.instance();
		GameManager gm = GameManager.instance();
		
		if(event.getDamager() instanceof Player && event.getEntity() instanceof Player){
			
			if(gm.isPvp() == false){
				event.setCancelled(true);
				return;
			}
			
			Player damager = (Player) event.getDamager();
			Player damaged = (Player) event.getEntity();
			
			WicPlayer wicDamager = pm.getWicPlayer(damager);
			WicPlayer wicDamaged = pm.getWicPlayer(damaged);
			
			if(wicDamaged != null && wicDamager != null){
				if(wicDamaged.isInTeamWith(wicDamager)){
					event.setCancelled(true);
				}else{
					// zombie poison sword and regen sword
					if(wicDamager.isClass(PlayerClassType.ZOMBIE) && damager.getItemInHand().getType().equals(Material.STONE_SWORD)){
						zombieSword(damager,damaged,event.getDamage());
					}
					
					// white walker freeze
					if(wicDamager.isClass(PlayerClassType.WHITE_WALKER) && damager.getItemInHand().getType().equals(Material.DIAMOND_SWORD)){
						whiteWalkerSword(damager,damaged);
					}
					
					// soldier 3 hearts damage
					if(wicDamager.isClass(PlayerClassType.SOLDIER) && damager.getItemInHand().getType().equals(Material.IRON_SWORD)){
						soldierDamage(event,damager,damaged);
					}
					
					// mestre one shot
					if(wicDamager.isClass(PlayerClassType.MESTRE) && damager.getItemInHand().getType().equals(Material.GOLD_SWORD) && wicDamaged.isClass(PlayerClassType.WHITE_WALKER)){
						mestreOneShort(damager,damaged);
					}
					
				}
			}
		}
	}

	private void handleArrow(EntityDamageByEntityEvent event){

		if(!event.isCancelled()){
			PlayersManager pm = PlayersManager.instance();
			GameManager gm = GameManager.instance();
			
			
			if(event.getEntity() instanceof Player && event.getDamager() instanceof Arrow){
				Projectile arrow = (Projectile) event.getDamager();
				final Player shot = (Player) event.getEntity();
				if(arrow.getShooter() instanceof Player){
					
					if(!gm.isPvp()){
						event.setCancelled(true);
						return;
					}
					
					final Player shooter = (Player) arrow.getShooter();
					WicPlayer wicDamager = pm.getWicPlayer(shooter);
					WicPlayer wicDamaged = pm.getWicPlayer(shot);

					if(wicDamager != null && wicDamaged != null){
						if(wicDamager.getState().equals(PlayerState.PLAYING) && wicDamager.isInTeamWith(wicDamaged)){
							event.setCancelled(true);
						}
					}
				}
			}
		}
		
	}
	
	private void zombieSword(Player damager, Player damaged, double damage){
		Long lastTime = lastZombiePoison.get(damager.getName());
		Long now = Calendar.getInstance().getTimeInMillis();
		if(lastTime == null || (lastTime != null && now-lastTime >= 10000)){
			lastZombiePoison.put(damager.getName(),now);
			double newHealth = damager.getHealth()+damage;
			newHealth = ((newHealth > 20) ? 20 : newHealth);
			damager.setHealth(newHealth);
			Effects.add(damaged, PotionEffectType.POISON, 80, 1);
			Sounds.play(damaged, Sound.BLAZE_HIT, 1, 0.5f);
			Sounds.play(damager, Sound.BLAZE_HIT, 1, 0.5f);
		}
	}
	
	private void whiteWalkerSword(Player damager, Player damaged){
		Long lastTime = lastWhiteWalkerFreeze.get(damager.getName());
		Long now = Calendar.getInstance().getTimeInMillis();
		if(lastTime == null || (lastTime != null && now-lastTime >= 10000)){
			lastWhiteWalkerFreeze.put(damager.getName(),now);
			Effects.add(damaged, PotionEffectType.SLOW, 80, 4);
			Effects.add(damaged, PotionEffectType.BLINDNESS, 80, 0);
			Sounds.play(damaged, Sound.ENDERMAN_DEATH, 1, 2);
			Sounds.play(damager, Sound.ENDERMAN_DEATH, 1, 2);
		}
	}

	
	private void mestreOneShort(Player damager, Player damaged) {
		Long lastTime = lastMestreOneShot.get(damager.getName());
		Long now = Calendar.getInstance().getTimeInMillis();
		if(lastTime == null || (lastTime != null && now-lastTime >= 10000)){
			lastMestreOneShot.put(damager.getName(),now);
			if(damaged.getVehicle() != null){
				damaged.getVehicle().eject();
			}
			damaged.setHealth(0);
			Sounds.play(damaged, Sound.FIREWORK_TWINKLE, 1, 2f);
			Sounds.play(damager, Sound.FIREWORK_TWINKLE, 1, 2f);
		}
	}
	
	private void soldierDamage(EntityDamageByEntityEvent event, Player damager, Player damaged){
		Long lastTime = lastSoldierDamage.get(damager.getName());
		Long now = Calendar.getInstance().getTimeInMillis();
		if(lastTime == null || (lastTime != null && now-lastTime >= 10000)){
			lastSoldierDamage.put(damager.getName(),now);
			double newHealth = damaged.getHealth()-6.0d;
			newHealth = ((newHealth >= 0) ? newHealth : 0);
			damaged.setHealth(newHealth);
			Sounds.play(damager, Sound.FALL_BIG, 1, 0.5f);
			Sounds.play(damager, Sound.FALL_BIG, 1, 0.6f);
			Sounds.play(damaged, Sound.FALL_BIG, 1, 0.5f);
			Sounds.play(damaged, Sound.FALL_BIG, 1, 0.6f);
		}
	}
}
