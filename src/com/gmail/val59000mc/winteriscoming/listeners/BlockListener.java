package com.gmail.val59000mc.winteriscoming.listeners;

import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Sounds;
import com.gmail.val59000mc.winteriscoming.classes.PlayerClassType;
import com.gmail.val59000mc.winteriscoming.configuration.Config;
import com.gmail.val59000mc.winteriscoming.dependencies.VaultManager;
import com.gmail.val59000mc.winteriscoming.objectives.Objective;
import com.gmail.val59000mc.winteriscoming.objectives.ObjectivesManager;
import com.gmail.val59000mc.winteriscoming.players.PlayersManager;
import com.gmail.val59000mc.winteriscoming.players.WicPlayer;
import com.gmail.val59000mc.winteriscoming.threads.RemoveAnvilThread;

import net.md_5.bungee.api.ChatColor;

public class BlockListener implements Listener{
	
	private Map<String,Long> lastGiantBreak;
	
	public BlockListener() {
		lastGiantBreak = Collections.synchronizedMap(new HashMap<String,Long>());
	}
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onBlockBreak(final BlockBreakEvent event){
		WicPlayer wicPlayer = PlayersManager.instance().getWicPlayer(event.getPlayer());
		
		if(wicPlayer == null){
			event.setCancelled(true);
			return;
		}
		
		handleBreakObjective(event,wicPlayer);
		handleGiantBreakBlock(event,wicPlayer);
	}
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onBlockPlace(final BlockPlaceEvent event){
		if(event.getBlock().getType().equals(Material.ANVIL)){
			RemoveAnvilThread.add(event.getBlock().getLocation().clone());
		}
	}


	private void handleBreakObjective(final BlockBreakEvent event,WicPlayer tPlayer){
		if(!event.isCancelled()){
			ObjectivesManager om = ObjectivesManager.instance();
			Objective objective = om.getObjective(event.getBlock());
			
			if(objective != null){

				boolean broken = om.tryBreakObjective(objective, tPlayer);
				event.setCancelled(true);
				
				if(broken){
					if(tPlayer.isOnline()){
						tPlayer.addMoney(rewardObjective(tPlayer.getPlayer()));
					}
					event.getBlock().setType(Material.AIR);
				}
				
			}
		}
	}
	
	private double rewardObjective(Player killer){
		double reward = 0;
		if(Config.isVaultLoaded){
			reward = VaultManager.addMoney(killer, Config.objectiveReward);
			killer.sendMessage(ChatColor.GREEN+"+"+reward+" "+ChatColor.WHITE+"HC");
			Sounds.play(killer, Sound.LEVEL_UP, 0.5f, 2f);
		}
		return reward;
	}


	private void handleGiantBreakBlock(BlockBreakEvent event,WicPlayer wicPlayer) {
		Logger.debug("-> BlockListener::handleGiantBreakBlock, player="+wicPlayer);
		if(!event.isCancelled()){
			if(wicPlayer.getPlayerClass() != null && wicPlayer.isClass(PlayerClassType.GIANT)){
				giantBreak(wicPlayer,event.getBlock());
			}
		}
		Logger.debug("<- BlockListener::handleGiantBreakBlock");
		
	}
	
	private void giantBreak(WicPlayer wicPlayer, Block block){
		
		if(wicPlayer.isOnline()){
			Player player = wicPlayer.getPlayer();
			Long lastTime = lastGiantBreak.get(wicPlayer.getName());
			Long now = Calendar.getInstance().getTimeInMillis();
			if(lastTime == null || (lastTime != null && now-lastTime >= 10000)){
				lastGiantBreak.put(wicPlayer.getName(),now);
				Sounds.playAll(player.getLocation(), Sound.ZOMBIE_WOODBREAK, 1, 0.7f);
				
				// break blocks
				Set<Block> blocks = new HashSet<Block>();
				blocks.add(block.getRelative(BlockFace.UP));
				blocks.add(block.getRelative(BlockFace.DOWN));
				blocks.add(block.getRelative(BlockFace.EAST));
				blocks.add(block.getRelative(BlockFace.SOUTH));
				blocks.add(block.getRelative(BlockFace.NORTH));
				blocks.add(block.getRelative(BlockFace.WEST));
				for(Block breakBlock : blocks){
					if(!breakBlock.getType().equals(Material.BEACON) && !breakBlock.getType().equals(Material.BEDROCK)){
						breakBlock.setType(Material.AIR);
					}
				}
			}
		}
		
		
		
		
	}

}
