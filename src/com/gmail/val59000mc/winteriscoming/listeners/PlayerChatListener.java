package com.gmail.val59000mc.winteriscoming.listeners;

import java.util.Set;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import com.gmail.val59000mc.winteriscoming.dependencies.PermissionsExManager;
import com.gmail.val59000mc.winteriscoming.players.PlayersManager;
import com.gmail.val59000mc.winteriscoming.players.WicPlayer;
import com.gmail.val59000mc.winteriscoming.players.WicTeam;

public class PlayerChatListener implements Listener{
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerChat(AsyncPlayerChatEvent event){
		
		WicPlayer evoPlayer = PlayersManager.instance().getWicPlayer(event.getPlayer());
		
		String message = event.getMessage();
		
		boolean isGlobalChat = evoPlayer.isGlobalChat();
		if(message.startsWith("!")){
			event.setMessage(message.substring(1, message.length()));
			isGlobalChat = true;
		}
		
		if(event.getPlayer().hasPermission("chatcontrol.chat.format.color")){
			event.setMessage(ChatColor.translateAlternateColorCodes('&', event.getMessage()));
		}
		
		String prefix = "§7"+PermissionsExManager.getStrippedPrefix(event.getPlayer());
		String suffix = ChatColor.translateAlternateColorCodes('&', PermissionsExManager.getSuffix(event.getPlayer()));
		event.setFormat(prefix+evoPlayer.getColor()+evoPlayer.getName()+": "+suffix+event.getMessage());
		
		if(evoPlayer != null && evoPlayer.getTeam() != null && !isGlobalChat){

			WicTeam team = evoPlayer.getTeam();
			
			Set<Player> recipients = event.getRecipients();
			recipients.clear();
			for(WicPlayer teammate : team.getMembers()){
				Player teamatePlayer = teammate.getPlayer();
				if(teamatePlayer != null){
					recipients.add(teamatePlayer);
				}
			}
			String teamPrefix = "§f["+team.getColor()+"TEAM"+"§f] §r";
			event.setFormat(teamPrefix+event.getFormat());
		}
		
	}
}
