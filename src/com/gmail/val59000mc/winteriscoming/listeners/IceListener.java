package com.gmail.val59000mc.winteriscoming.listeners;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockFormEvent;
import org.bukkit.event.block.BlockFromToEvent;

public class IceListener implements Listener{
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onIceForm(final BlockFormEvent event){
		if(event.getNewState().getType().equals(Material.ICE)){
			event.setCancelled(true);
		}
	}
	

	@EventHandler(priority=EventPriority.HIGHEST)
	public void onIceForm(final BlockFromToEvent event){
		if(event.getToBlock().getType().equals(Material.ICE)){
			event.setCancelled(true);
		}
	}

}
