package com.gmail.val59000mc.winteriscoming.listeners;

import org.bukkit.entity.Horse;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.spigotmc.event.entity.EntityMountEvent;

import com.gmail.val59000mc.winteriscoming.classes.PlayerClassManager;
import com.gmail.val59000mc.winteriscoming.players.PlayersManager;
import com.gmail.val59000mc.winteriscoming.players.WicPlayer;

public class PlayerInteractListener implements Listener {

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onRightClickEntity(EntityMountEvent event) {
		
		if(event.getEntity() instanceof Player){
			
			WicPlayer wicPlayer = PlayersManager.instance().getWicPlayer((Player) event.getEntity());
			
			if(wicPlayer == null || !wicPlayer.isOnline()){
				return;
			}
			
			handleHorseMount(event,wicPlayer);
			
		}
		
		
		
	}
	
	public void handleHorseMount(EntityMountEvent event, WicPlayer wicPlayer){
		if(event.getMount() instanceof Horse){
			Horse horse = (Horse) event.getMount();
			if(!PlayerClassManager.instance().isWhiteWalkerHorse(wicPlayer,horse)){
				event.setCancelled(true);
			}
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerDropItem(PlayerDropItemEvent event) {
		event.getItemDrop().remove();	
	}
}
