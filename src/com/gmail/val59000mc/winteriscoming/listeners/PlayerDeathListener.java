package com.gmail.val59000mc.winteriscoming.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Sounds;
import com.gmail.val59000mc.winteriscoming.WIC;
import com.gmail.val59000mc.winteriscoming.configuration.Config;
import com.gmail.val59000mc.winteriscoming.dependencies.VaultManager;
import com.gmail.val59000mc.winteriscoming.game.GameManager;
import com.gmail.val59000mc.winteriscoming.game.GameState;
import com.gmail.val59000mc.winteriscoming.players.PlayerState;
import com.gmail.val59000mc.winteriscoming.players.PlayersManager;
import com.gmail.val59000mc.winteriscoming.players.WicPlayer;

import net.md_5.bungee.api.ChatColor;

public class PlayerDeathListener implements Listener {
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerDeath(final PlayerDeathEvent event) {

		WicPlayer wicPlayer = PlayersManager.instance().getWicPlayer(event.getEntity());
		
		if(wicPlayer == null || !wicPlayer.isOnline()){
			return;
		}
		
		handleDeathPlayer(event,wicPlayer);
		
	}
	
	public void handleDeathPlayer(final PlayerDeathEvent event, WicPlayer dead){
		PlayersManager pm = PlayersManager.instance();
		
		// Add death score to dead player and kill score to killer if playing
		if(GameManager.instance().isState(GameState.PLAYING)){
			dead.addDeath();
			
			WicPlayer killer = (dead.getPlayer().getKiller() == null) ? null : pm.getWicPlayer(dead.getPlayer().getKiller());
			
			if(killer != null && killer.isOnline()){
				killer.addKill();
				killer.addMoney(rewardKill(killer.getPlayer()));
			}
			
			Bukkit.getScheduler().runTaskLater(WIC.getPlugin(), new Runnable() {
				
				public void run() {
			        event.getEntity().spigot().respawn();
				}
			}, 20);
			

			// clear drops
			event.getDrops().clear();
		}
		
		
		
		
	}
	
	private double rewardKill(Player killer) {
		double reward = 0;
		if(Config.isVaultLoaded){
			reward = VaultManager.addMoney(killer, Config.killReward);
			killer.sendMessage(ChatColor.GREEN+"+"+reward+" "+ChatColor.WHITE+"HC");
			Sounds.play(killer, Sound.LEVEL_UP, 0.5f, 2f);
		}
		return reward;
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerRespawn(PlayerRespawnEvent event) {
		Logger.debug("-> PlayerDeathListener::onPlayerRespawn, player="+event.getPlayer().getName());
		
		WicPlayer wicPlayer = PlayersManager.instance().getWicPlayer(event.getPlayer());
		
		if(wicPlayer == null){
			return;
		}
		
		handleRespawnPlayer(event,wicPlayer);
		
		Logger.debug("<- PlayerDeathListener::onPlayerRespawn");
	}
	
	public void handleRespawnPlayer(PlayerRespawnEvent event, WicPlayer wicPlayer){
		Logger.debug("-> PlayerDeathListener::handleRespawnPlayer, player="+wicPlayer);

		final String name = wicPlayer.getName();
		
		if(wicPlayer.getSpawnPoint() != null){
			Location location = wicPlayer.getSpawnPoint();
			event.setRespawnLocation(location);
			location.getBlock().setType(Material.AIR);
			location.clone().add(0,1,0).getBlock().setType(Material.AIR);
		}
		
		switch(GameManager.instance().getState()){
			case PLAYING:
				Bukkit.getScheduler().runTaskLater(WIC.getPlugin(), new Runnable() {
					
					@Override
					public void run() {
						Logger.debug("teamSpawnPlayer");
						WicPlayer p = PlayersManager.instance().getWicPlayer(name);
						if(p != null){
							if(p.isState(PlayerState.PLAYING))
								PlayersManager.instance().teamSpawnPlayer(p);
							else
								PlayersManager.instance().spectatePlayer(p);
						}
						
					}
				}, 1);
				break;
			case WAITING:
					Bukkit.getScheduler().runTaskLater(WIC.getPlugin(), new Runnable() {
					
					@Override
					public void run() {
						Logger.debug("waitPlayer");
						WicPlayer p = PlayersManager.instance().getWicPlayer(name);
						if(p != null){
							PlayersManager.instance().waitPlayer(p);
						}
						
					}
				}, 1);
				break;
			case LOADING:
			case STARTING:
			case ENDED:
			default:
				break;
		}
		

		Logger.debug("<- PlayerDeathListener::handleRespawnPlayer");
	}
	
}
