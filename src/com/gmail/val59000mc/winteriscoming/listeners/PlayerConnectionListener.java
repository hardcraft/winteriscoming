package com.gmail.val59000mc.winteriscoming.listeners;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;

import com.gmail.val59000mc.winteriscoming.WIC;
import com.gmail.val59000mc.winteriscoming.game.GameManager;
import com.gmail.val59000mc.winteriscoming.i18n.I18n;
import com.gmail.val59000mc.winteriscoming.players.PlayersManager;
import com.gmail.val59000mc.winteriscoming.players.WicPlayer;
import com.gmail.val59000mc.winteriscoming.threads.KillDisconnectedPlayerThread;

public class PlayerConnectionListener implements Listener{
	
	private static Set<String> kickedPlayersNames = Collections.synchronizedSet(new HashSet<String>());
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerLogin(PlayerLoginEvent event){
		Player player = event.getPlayer();
		
		boolean isAllowedToJoin = PlayersManager.instance().isPlayerAllowedToJoin(player);
		if(isAllowedToJoin){
			event.setResult(Result.ALLOWED);
		}else{
			kickedPlayersNames.add(player.getName());
			event.setKickMessage(I18n.get("player.not-allowed-to-join", player));
			event.setResult(Result.KICK_OTHER);
		}
	}
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerJoin(final PlayerJoinEvent event){
		Bukkit.getScheduler().runTaskLater(WIC.getPlugin(), new Runnable(){

			@Override
			public void run() {
				PlayersManager.instance().playerJoinsTheGame(event.getPlayer());
			}
			
		}, 1);
		
	}
	
	
	
	
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerDisconnect(PlayerQuitEvent event){
		
		Player player = event.getPlayer();
		
		// Kicked players must be ignore by the rest of this method
		if(kickedPlayersNames.contains(player.getName())){
			kickedPlayersNames.remove(player.getName());
			return;
		}
			
			
		GameManager gm = GameManager.instance();
		switch(gm.getState()){
			case WAITING:
			case STARTING:
				PlayersManager.instance().removePlayer(player.getName());
				break;
			case PLAYING:
				WicPlayer wicPlayer = PlayersManager.instance().getWicPlayer(player);
				if(wicPlayer != null && wicPlayer.isPlaying()){
					KillDisconnectedPlayerThread.start(player.getName());
				}else{
					PlayersManager.instance().removePlayer(player.getName());
				}
				break;
			case LOADING:
			case ENDED:
			default:
				break;
		}
	}
	
}
