package com.gmail.val59000mc.winteriscoming;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.winteriscoming.game.GameManager;

public class WIC extends JavaPlugin{
	
	private static WIC pl;
	
	
	public void onEnable(){
		pl = this;
	
		// Blocks players joins while loading the plugin
		Bukkit.getServer().setWhitelist(true);
		saveDefaultConfig();
		
		Logger.setColoredPrefix(ChatColor.WHITE+"["+ChatColor.GREEN+"WinterIsComing"+ChatColor.WHITE+"]"+ChatColor.RESET+" ");
		Logger.setStrippedPrefix("[WinterIsComing] ");
		
		Bukkit.getScheduler().runTaskLater(this, new Runnable(){
			
			@Override
			public void run() {
				GameManager.instance().loadGame();
				
				// Unlock players joins and rely on UhcPlayerJoinListener
				Bukkit.getServer().setWhitelist(false);
			}
			
		}, 1);
		
		
	}
	
	public static WIC getPlugin(){
		return pl;
	}
	
	public void onDisable(){
	}
}
