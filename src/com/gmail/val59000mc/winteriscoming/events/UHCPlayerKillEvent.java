package com.gmail.val59000mc.winteriscoming.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.gmail.val59000mc.winteriscoming.players.WicPlayer;

public final class UHCPlayerKillEvent extends Event{
	
	private static final HandlerList handlers = new HandlerList();
	private WicPlayer killer;
	private WicPlayer killed;
	
	public UHCPlayerKillEvent(WicPlayer killer, WicPlayer killed){
		this.killed = killer;
		this.killed = killed;
	}
	
	public WicPlayer getKiller(){
		return killer;
	}
	
	public WicPlayer getKilled(){
		return killed;
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
}
