package com.gmail.val59000mc.winteriscoming.events;

import java.util.Set;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.gmail.val59000mc.winteriscoming.players.WicPlayer;

public class UhcWinEvent extends Event{

	private static final HandlerList handlers = new HandlerList();
	private Set<WicPlayer> winners;
	
	public UhcWinEvent(Set<WicPlayer> winners){
		this.winners = winners;
	}

	public Set<WicPlayer> getWinners(){
		return winners;
	}
	
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

}
