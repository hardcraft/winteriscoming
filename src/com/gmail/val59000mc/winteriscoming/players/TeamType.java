package com.gmail.val59000mc.winteriscoming.players;

import org.bukkit.ChatColor;

import com.gmail.val59000mc.winteriscoming.configuration.Config;

public enum TeamType {
	NIGHT_WATCH(ChatColor.GREEN,"team.NIGHT_WATCH."+Config.map),
	ARMY_OF_THE_DEAD(ChatColor.RED,"team.ARMY_OF_THE_DEAD."+Config.map);
	
	private String code;
	private ChatColor color;

	private TeamType(ChatColor color, String code) {
		this.color = color;
		this.code = code;
	}

	public String getCode() {
		return code;
	}

	public ChatColor getColor() {
		return color;
	}
	
	
	
	
}
