package com.gmail.val59000mc.winteriscoming.players;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffectType;

import com.gmail.val59000mc.spigotutils.Effects;
import com.gmail.val59000mc.spigotutils.Inventories;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Sounds;
import com.gmail.val59000mc.spigotutils.Texts;
import com.gmail.val59000mc.winteriscoming.classes.PlayerClassManager;
import com.gmail.val59000mc.winteriscoming.configuration.Config;
import com.gmail.val59000mc.winteriscoming.dependencies.VaultManager;
import com.gmail.val59000mc.winteriscoming.game.EndCause;
import com.gmail.val59000mc.winteriscoming.game.GameManager;
import com.gmail.val59000mc.winteriscoming.game.GameState;
import com.gmail.val59000mc.winteriscoming.i18n.I18n;
import com.gmail.val59000mc.winteriscoming.objectives.ObjectivesManager;
import com.gmail.val59000mc.winteriscoming.threads.BridgeDoorExplodeThread;
import com.gmail.val59000mc.winteriscoming.threads.RemainingTimeThread;
import com.gmail.val59000mc.winteriscoming.threads.UpdateScoreboardThread;
import com.gmail.val59000mc.winteriscoming.titles.TitleManager;
import com.gmail.val59000mc.winteriscoming.tutorial.SIGTutorial;

import net.md_5.bungee.api.ChatColor;

public class PlayersManager {
	private static PlayersManager instance;
	
	private List<WicPlayer> players;
	private List<WicTeam> teams;
	
	// static
	
	public static PlayersManager instance(){
		if(instance == null){
			instance = new PlayersManager();
		}
		
		return instance;
	}
	
	// constructor 
	
	private PlayersManager(){
		players = Collections.synchronizedList(new ArrayList<WicPlayer>());
		teams = Collections.synchronizedList(new ArrayList<WicTeam>());

		teams.add(new WicTeam(TeamType.ARMY_OF_THE_DEAD));
		teams.add(new WicTeam(TeamType.NIGHT_WATCH));
	}
	
	
	// Accessors
	
	public WicPlayer getWicPlayer(Player player){
		return getWicPlayer(player.getName());
	}
	
	public WicPlayer getWicPlayer(String name){
		for(WicPlayer uhcPlayer : getPlayers()){
			if(uhcPlayer.getName().equals(name))
				return uhcPlayer;
		}
		
		return null;
	}
	
	public synchronized List<WicPlayer> getPlayers(){
		return players;
	}
	
	public synchronized List<WicTeam> getTeams(){
		return teams;
	}
	
	public synchronized WicTeam getTeam(TeamType type){
		for(WicTeam team : getTeams()){
			if(team.getType().equals(type)){
				return team;
			}
		}
		return null;
	}

	public boolean canAddPlayerToTeam(WicTeam evoTeam) {
		return evoTeam.getMembers().size() < getMinMembersTeam().getMembers().size()+1;
	}
	
	private WicTeam getMinMembersTeam(){
		WicTeam min = getTeams().get(0);
		for(WicTeam team : getTeams()){
			if(team.getMembers().size() < min.getMembers().size()){
				min = team;
			}
		}
		return min;
	}
	
	// Methods 
	
	public synchronized WicPlayer addPlayer(Player player){
		WicPlayer newPlayer = new WicPlayer(player);
		getPlayers().add(newPlayer);
		return newPlayer;
	}
	
	public synchronized void removePlayer(Player player){
		removePlayer(player.getName());
	}
	
	public synchronized void removePlayer(String name){
		WicPlayer wicPlayer = getWicPlayer(name);
		if(wicPlayer != null){
			wicPlayer.leaveTeam();
			getPlayers().remove(wicPlayer);
		}
	}
	
	public boolean isPlaying(Player player){
		WicPlayer wicPlayer = getWicPlayer(player);
		if(wicPlayer != null){
			return wicPlayer.getState().equals(PlayerState.PLAYING);
		}
		return false;
	}

	public boolean isPlayerAllowedToJoin(Player player){
		Logger.debug("-> PlayersManager::isPlayerAllowedToJoin, player="+player.getName());
		GameManager gm = GameManager.instance();
		
		switch(gm.getState()){
				
			case WAITING:
			case PLAYING:	
				Logger.debug("<- PlayersManager::isPlayerAllowedToJoin, player="+player.getName()+", allowed=true, gameState="+gm.getState());
				return true;
			case STARTING:
			case LOADING:
			case ENDED:
			default:
				Logger.debug("<- PlayersManager::isPlayerAllowedToJoin, player="+player.getName()+", allowed=false, gameState="+gm.getState());
				return false;
		}
	}

	public void playerJoinsTheGame(Player player) {
		Logger.debug("-> PlayersManager::playerJoinsTheGame, player="+player.getName());
		WicPlayer wicPlayer = getWicPlayer(player);
		if(wicPlayer == null){
			wicPlayer = addPlayer(player);
		}
		
		GameState gameState = GameManager.instance().getState();
		switch(gameState){
			case WAITING:
				wicPlayer.setState(PlayerState.WAITING);
				break;
			case STARTING:
			case LOADING:
			case ENDED:
				wicPlayer.setState(PlayerState.DEAD);
				break;
			case PLAYING:
				if(!wicPlayer.isState(PlayerState.PLAYING)){
					wicPlayer.setState(PlayerState.DEAD);
				}
				break;
				
		}
			
		switch(wicPlayer.getState()){
			case WAITING:
				Logger.debug("waitPlayer");
				waitPlayer(wicPlayer);
				wicPlayer.sendI18nMessage("player.welcome");
				Logger.broadcast(
					I18n.get("player.joined")
						.replace("%player%",wicPlayer.getName())
						.replace("%count%", String.valueOf(getPlayers().size()))
						.replace("%total%",  String.valueOf(Config.maxPlayers))
				);
				if(Config.isBountifulApiLoaded){
					TitleManager.sendTitle(player, ChatColor.GREEN+"Winter Is Coming", 20, 20, 20);
				}
				break;
			case PLAYING:
				Logger.debug("relogPlayer");
				relogPlayer(wicPlayer);
				break;
			case DEAD:
				Logger.debug("spectatePlayer");
				spectatePlayer(wicPlayer);
				break;
		}

		wicPlayer.refreshNameTag();
		refreshVisiblePlayers();
	}

	public void waitPlayer(WicPlayer wicPlayer){

		wicPlayer.goToLobby();
		
		if(wicPlayer.isOnline()){
			Player player = wicPlayer.getPlayer();
			Inventories.clear(player);
			if(Config.isSIGLoaded){
				SIGTutorial.giveTutorialInventory(player);
			}
			if(Bukkit.getOnlinePlayers().size()>Config.maxPlayers){
				wicPlayer.sendI18nMessage("player.full");
			}
			player.setGameMode(GameMode.ADVENTURE);
			Effects.addPermanent(player, PotionEffectType.SATURATION, 0);
			Effects.addPermanent(player, PotionEffectType.NIGHT_VISION, 0);
			Effects.addPermanent(player, PotionEffectType.SPEED, 0);
			player.setHealth(20);
			player.setExhaustion(20);
			player.setFoodLevel(20);
			player.setExp(0);
		}
		
	}
	
	/**
	 * Relog a playing player in the game
	 * @param wicPlayer
	 */
	public void relogPlayer(WicPlayer wicPlayer){
		
		if(wicPlayer.isOnline() && GameManager.instance().isState(GameState.PLAYING) && wicPlayer.getTeam() != null){
			Logger.broadcast(I18n.get("player.reconnect").replace("%player%", wicPlayer.getName()));
		}else{
			spectatePlayer(wicPlayer);
		}
		
	}
	
	/**
	 * Respawn a playing player in his team
	 * @param wicPlayer
	 */
	public void teamSpawnPlayer(WicPlayer wicPlayer){
		if(wicPlayer.isOnline() && wicPlayer.getTeam() != null){
			
			Player player = wicPlayer.getPlayer();
			
			player.setGameMode(GameMode.SURVIVAL);
			player.setHealth(20);
			player.setExhaustion(20);
			player.setFoodLevel(20);
			player.setExp(0);

			PlayerClassManager.instance().assignRandomAvailableClassToPlayer(wicPlayer);
		}
	}
	
	public void spectatePlayer(WicPlayer wicPlayer){
		
		wicPlayer.setState(PlayerState.DEAD);
		wicPlayer.sendI18nMessage("player.spectate");
		UpdateScoreboardThread.add(wicPlayer);
		
		if(wicPlayer.isOnline()){
			
			Player player = wicPlayer.getPlayer();
			Inventories.clear(player);
			player.setGameMode(GameMode.SPECTATOR);
			player.teleport(Config.mapLocation);
			
		}
	}
	
	public WicTeam getAttackTeam(){
		for(WicTeam team : getTeams()){
			if(team.is(TeamType.ARMY_OF_THE_DEAD)){
				return team;
			}
		}
		
		// returning null should not happen normally
		return null;
	}
	
	public WicTeam getDefenseTeam(){
		for(WicTeam team : getTeams()){
			if(team.is(TeamType.NIGHT_WATCH)){
				return team;
			}
		}
		
		// returning null should not happen normally
		return null;
	}

	public boolean isRemainingPlayers() {
		// to do
		return true;
	}
	
	public Set<WicPlayer> getPlayingPlayers() {
		Set<WicPlayer> playingPlayers = new HashSet<WicPlayer>();
		for(WicPlayer p : getPlayers()){
			if(p.getState().equals(PlayerState.PLAYING) && p.isOnline()){
				playingPlayers.add(p);
			}
		}
		return playingPlayers;
	}

	public List<WicPlayer> getWaitingPlayers() {
		List<WicPlayer> waitingPlayers = new ArrayList<WicPlayer>();
		for(WicPlayer p : getPlayers()){
			if(p.getState().equals(PlayerState.WAITING) && p.isOnline()){
				waitingPlayers.add(p);
			}
		}
		return waitingPlayers;
	}
	
	public void startAllPlayers() {
		Logger.debug("-> PlayersManager::startAllPlayers");
		assignRandomTeamsToPlayers();
		refreshVisiblePlayers();
		for(WicPlayer wicPlayer : getPlayers()){
			if(wicPlayer.getTeam() == null){
				spectatePlayer(wicPlayer);
			}else{
				startPlayer(wicPlayer);
			}
		}
		UpdateScoreboardThread.start();
		Logger.debug("<- PlayersManager::startAllPlayers");
	}
	
	public void startPlayer(WicPlayer wicPlayer){
		wicPlayer.setGlobalChat(false);
		wicPlayer.teleportToSpawnPoint();
		teamSpawnPlayer(wicPlayer);
		wicPlayer.setState(PlayerState.PLAYING);
		if(Config.isBountifulApiLoaded && wicPlayer.isOnline() && wicPlayer.getTeam() != null){
			TitleManager.sendTitle(wicPlayer.getPlayer(), I18n.get(wicPlayer.getTeam().getType().getCode()+".side", wicPlayer.getPlayer()), 20, 20, 20);
		}
		UpdateScoreboardThread.add(wicPlayer);
	}
	
	
	private void refreshVisiblePlayers() {
		for(Player player : Bukkit.getOnlinePlayers()){
			for(Player onePlayer : Bukkit.getOnlinePlayers()){
				player.hidePlayer(onePlayer);
				player.showPlayer(onePlayer);
			}
		}
	}

	private void assignRandomTeamsToPlayers(){
		Logger.debug("-> PlayersManager::assignRandomTeamsToPlayers");
		
		synchronized(players){
			// Shuffle players who will play (up to maxPlayers)
			List<WicPlayer> playersWhoWillPlay = new ArrayList<WicPlayer>(getWaitingPlayers().subList(0, Config.maxPlayers > players.size() ? players.size() : Config.maxPlayers));
			List<WicPlayer> playersWhoWillNotPlay = new ArrayList<WicPlayer>();
			if(players.size() > Config.maxPlayers){
				playersWhoWillNotPlay = new ArrayList<WicPlayer>(getWaitingPlayers().subList(Config.maxPlayers, players.size()));	
			}
			this.players = new ArrayList<WicPlayer>();
			Collections.shuffle(playersWhoWillPlay);
			this.players.addAll(playersWhoWillPlay);
			this.players.addAll(playersWhoWillNotPlay);
			
			for(WicPlayer tPlayer : playersWhoWillNotPlay){
				tPlayer.leaveTeam();
			}
		}
		
		WicTeam attackTeam = getAttackTeam();
		WicTeam defenseTeam = getDefenseTeam();
		
		
		int nPlayer = 0;
		for(WicPlayer evoPlayer : getWaitingPlayers()){
			nPlayer++;
			
			if(nPlayer <= Config.maxPlayers){
				
				WicTeam team = evoPlayer.getTeam();
				
				if(evoPlayer.getTeam() == null){
					team = getMinMembersTeam();
					team.addPlayer(evoPlayer);
				}

				if(Config.isBountifulApiLoaded && evoPlayer.isOnline())
					TitleManager.sendTitle(evoPlayer.getPlayer(), I18n.get("player."+evoPlayer.getTeam().getType().toString().toLowerCase(),evoPlayer.getPlayer()), 10, 20 , 10);
				
			}
			
			evoPlayer.refreshNameTag();
		}
		
		attackTeam.setSpawnPoint(Config.spawn1Attack);
		defenseTeam.setSpawnPoint(Config.spawn1Defense);

		Logger.debug("<- PlayersManager::assignRandomTeamsToPlayers");
	}

	public void endAllPlayers(EndCause cause) {
		
		WicTeam winningTeam = null;
		switch(cause){
			case ATTACK_WIN:
				// attack defensers
				winningTeam = getAttackTeam();
				break;
			case DEFENSE_WIN:
				// reward defensers
				winningTeam = getDefenseTeam();
				break;
			case NO_MORE_PLAYERS:
			default:
				// nothing special to do
				break;
		}
		
		if(winningTeam != null){
			for(WicPlayer wicPlayer : winningTeam.getMembers()){
				if(wicPlayer.isOnline()){
					wicPlayer.addMoney(VaultManager.addMoney(wicPlayer.getPlayer(), Config.winReward));
				}
			}
		}
		

		for(WicPlayer wicPlayer : getPlayers()){
			wicPlayer.setGlobalChat(true);
			if(wicPlayer.isOnline()){
				Player player = wicPlayer.getPlayer();
				spectatePlayer(wicPlayer);
				printEndMessage(wicPlayer, winningTeam);
			}
		}
		
		Sounds.playAll(Sound.ENDERDRAGON_GROWL,0.8f,2);
		
		
	}
	
	private void printEndMessage(WicPlayer rPlayer, WicTeam winningTeam){
		if(rPlayer.isOnline()){
			Player player = rPlayer.getPlayer();
			
			if(winningTeam == null){
				Texts.tellraw(player, I18n.get("stats.end.draw",player)
						.replace("%kills%", String.valueOf(rPlayer.getKills()))
						.replace("%deaths%", String.valueOf(rPlayer.getDeaths()))
						.replace("%hardcoins%", String.valueOf(rPlayer.getMoney()))
				);
			}else{
				Texts.tellraw(player, I18n.get("stats.end",player)
						.replace("%kills%", String.valueOf(rPlayer.getKills()))
						.replace("%deaths%", String.valueOf(rPlayer.getDeaths()))
						.replace("%hardcoins%", String.valueOf(rPlayer.getMoney()))
						.replace("%winner%", winningTeam.getColor()+winningTeam.getI18nName(player))
				);
			}
		}
	}

	public void moveToNextDefenseLine() {
		Logger.debug("-> PlayersManager::moveToNextDefenseLine");

		RemainingTimeThread.addRemainingTime(Config.breakDefenseLineTime);
		getAttackTeam().sendI18nMessage("objectives.defense-line-breached");
		getDefenseTeam().sendI18nMessage("objectives.defense-line-retreat");
		
		Sounds.playAll(Sound.ENDERDRAGON_GROWL, 1, 2);
		
		switch(ObjectivesManager.instance().getCurrentObjectiveLevel()){
			case TWO:
				moveToForestDefenseLine();
				break;
			case THREE:
				moveToWallDefenseLine();
				break;
			default:
				break;
		}
		Logger.debug("<- PlayersManager::moveToNextDefenseLine");
	}
	
	private void moveToForestDefenseLine(){
		Logger.debug("-> PlayersManager::moveToForestDefenseLine");
		getDefenseTeam().setSpawnPoint(Config.spawn2Defense);
		getAttackTeam().setSpawnPoint(Config.spawn2Attack);
		
		for(WicPlayer wicPlayer : getDefenseTeam().getMembers()){
			if(wicPlayer.isOnline() && wicPlayer.isPlaying()){
				if(Config.isBountifulApiLoaded){
					Player player = wicPlayer.getPlayer();
					TitleManager.sendTitle(player, I18n.get("objectives.defend.2."+Config.map,player), 20, 20, 20);
				}
				wicPlayer.teleportToSpawnPoint();
			}
		}
		
		for(WicPlayer wicPlayer : getAttackTeam().getMembers()){
			if(wicPlayer.isOnline() && wicPlayer.isPlaying()){
				if(Config.isBountifulApiLoaded){
					Player player = wicPlayer.getPlayer();
					TitleManager.sendTitle(player, I18n.get("objectives.attack.2."+Config.map,player), 20, 20, 20);
				}
			}
		}
		
		BridgeDoorExplodeThread.start();
		Logger.debug("<- PlayersManager::moveToForestDefenseLine");
	}
	
	private void moveToWallDefenseLine(){
		Logger.debug("-> PlayersManager::moveToWallDefenseLine");
		getDefenseTeam().setSpawnPoint(Config.spawn3Defense);
		getAttackTeam().setSpawnPoint(Config.spawn3Attack);
		
		for(WicPlayer wicPlayer : getDefenseTeam().getMembers()){
			if(wicPlayer.isOnline() && wicPlayer.isPlaying()){
				if(Config.isBountifulApiLoaded){
					Player player = wicPlayer.getPlayer();
					TitleManager.sendTitle(player, I18n.get("objectives.defend.3."+Config.map,player), 20, 20, 20);
				}
				wicPlayer.teleportToSpawnPoint();
			}
		}
		
		for(WicPlayer wicPlayer : getAttackTeam().getMembers()){
			if(wicPlayer.isOnline() && wicPlayer.isPlaying()){
				if(Config.isBountifulApiLoaded){
					Player player = wicPlayer.getPlayer();
					TitleManager.sendTitle(player, I18n.get("objectives.attack.3."+Config.map,player), 20, 20, 20);
				}
			}
		}
		
		
		Logger.debug("<- PlayersManager::moveToWallDefenseLine");
		
	}
	
	
}
