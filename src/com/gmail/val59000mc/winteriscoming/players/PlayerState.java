package com.gmail.val59000mc.winteriscoming.players;

public enum PlayerState {
  WAITING,
  PLAYING,
  DEAD;
}
