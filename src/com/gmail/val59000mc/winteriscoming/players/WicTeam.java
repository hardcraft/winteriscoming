package com.gmail.val59000mc.winteriscoming.players;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.winteriscoming.configuration.Config;
import com.gmail.val59000mc.winteriscoming.i18n.I18n;

public class WicTeam {

	private List<WicPlayer> members;
	private TeamType type;
	private Location spawnPoint;
	
	public WicTeam(TeamType type) {
		this.members = new ArrayList<WicPlayer>();
		this.type = type;
	}
	
	public TeamType getType() {
		return type;
	}

	public void setType(TeamType type) {
		this.type = type;
	}


	public void sendI18nMessage(String code) {
		for(WicPlayer wicPlayer: members){
			wicPlayer.sendI18nMessage(code);
		}
	}
	
	public String getI18nName(Player player){
		return I18n.get("team."+getType().toString()+"."+Config.map, player);
	}
	
	public boolean contains(WicPlayer player){
		return members.contains(player);
	}
	
	public synchronized List<WicPlayer> getMembers(){
		return members;
	}

	public List<WicPlayer> getPlayingMembers(){
		List<WicPlayer> playingMembers = new ArrayList<WicPlayer>();
		for(WicPlayer uhcPlayer : getMembers()){
			if(uhcPlayer.getState().equals(PlayerState.PLAYING)){
				playingMembers.add(uhcPlayer);
			}
		}
		return playingMembers;
	}
	
	public synchronized List<String> getMembersNames(){
		List<String> names = new ArrayList<String>();
		for(WicPlayer player : getMembers()){
			names.add(player.getName());
		}
		return names;
	}
	
	public void addPlayer(WicPlayer wicPlayer){
		wicPlayer.leaveTeam();
		wicPlayer.setTeam(this);
		getMembers().add(wicPlayer);
	}
	
	public void leave(WicPlayer wicPlayer){
		getMembers().remove(wicPlayer);
	}
	
	public boolean isOnline(){
		for(WicPlayer wicPlayer : getMembers()){
			if(wicPlayer.isOnline()){
				return true;
			}
		}
		return false;
	}

	
	public List<WicPlayer> getOtherMembers(WicPlayer excludedPlayer){
		List<WicPlayer> otherMembers = new ArrayList<WicPlayer>();
		for(WicPlayer uhcPlayer : getMembers()){
			if(!uhcPlayer.equals(excludedPlayer))
				otherMembers.add(uhcPlayer);
		}
		return otherMembers;
	}

	
	public void setSpawnPoint(Location loc){
		this.spawnPoint = loc;
	}
	
	public Location getSpawnPoint(){
		return spawnPoint;
	}

	public Boolean is(TeamType type) {
		return getType().equals(type);
	}

	public ChatColor getColor() {
		return getType().getColor();
	}

	public boolean canAddPlayer() {
		if(getMembers().size() < Config.minPlayers/2){
			return true;
		}else{
			return PlayersManager.instance().canAddPlayerToTeam(this);
		}
	}
	
}
