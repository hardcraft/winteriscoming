package com.gmail.val59000mc.winteriscoming.players;


import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Numbers;
import com.gmail.val59000mc.winteriscoming.classes.PlayerClass;
import com.gmail.val59000mc.winteriscoming.classes.PlayerClassType;
import com.gmail.val59000mc.winteriscoming.configuration.Config;
import com.gmail.val59000mc.winteriscoming.dependencies.PermissionsExManager;
import com.gmail.val59000mc.winteriscoming.i18n.I18n;

import ca.wacos.nametagedit.NametagAPI;

public class WicPlayer {
	private String name;
	private WicTeam team;
	private PlayerState state;
	private boolean globalChat;
	private PlayerClass playerClass;
	private int kills;
	private int deaths;
	private double money;
	
	
	// Constructor
	
	public WicPlayer(Player player){
		this.name = player.getName();
		this.team = null;
		this.state = PlayerState.WAITING;
		this.globalChat = true;
		this.playerClass = null;
		this.kills = 0;
		this.deaths = 0;
		this.money = 0;
	}
	
	
	
	// Accessors

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public synchronized WicTeam getTeam() {
		return team;
	}
	public synchronized void setTeam(WicTeam team) {
		this.team = team;
	}
	public PlayerState getState() {
		return state;
	}
	public void setState(PlayerState state) {
		this.state = state;
	}
	public double getMoney() {
		return Numbers.round(money,2);
	}
	public void addMoney(double money) {
		this.money += money;
	}
	public boolean isGlobalChat() {
		return globalChat;
	}
	public void setGlobalChat(boolean globalChat) {
		this.globalChat = globalChat;
	}
	public PlayerClass getPlayerClass() {
		return playerClass;
	}
	public void setPlayerClass(PlayerClass playerClass) {
		this.playerClass = playerClass;
	}
	public int getKills() {
		return kills;
	}
	public void addKill() {
		this.kills++;
	}
	public int getDeaths() {
		return deaths;
	}
	public void addDeath() {
		this.deaths++;
	}
	
	
	// Methods




	public Player getPlayer(){
		return Bukkit.getPlayer(name);
	}
	
	public Boolean isOnline(){
		return Bukkit.getPlayer(name) != null;
	}
	
	public boolean isInTeamWith(WicPlayer player){
		return (team != null && team.equals(player.getTeam()));
	}
	
	public void teleportToSpawnPoint() {
		if(isOnline() && getSpawnPoint() != null){
			getPlayer().teleport(getSpawnPoint());
		}
	}
	
	public void goToLobby(){
		if(isOnline()){
			getPlayer().teleport(Config.lobbyLocation);
		}
	}
	
	public void leaveTeam(){
		if(team != null){
			team.leave(this);
			team = null;
		}
	}

	public void sendI18nMessage(String code) {
		if(isOnline()){
			Logger.sendMessage(getPlayer(), I18n.get(code,getPlayer()));
		}
	}
	
	
	public String toString(){
		return "[name='"+getName()+
				"',team='"+((getTeam() == null) ? null : getTeam().getType())+"'"+
				",class='"+((getPlayerClass() == null) ? null : getPlayerClass().getType())+"']";
	}



	public boolean isClass(PlayerClassType type) {
		return getPlayerClass() != null && getPlayerClass().getType().equals(type);
	}

	public boolean isState(PlayerState state) {
		return getState().equals(state);
	}



	public boolean isTeam(TeamType type) {
		return getTeam() != null && getTeam().getType().equals(type);
	}



	public Location getSpawnPoint() {
		if(team != null){
			return team.getSpawnPoint();
		}
		return null;
	}



	public boolean isPlaying() {
		return isOnline() && isState(PlayerState.PLAYING);
	}



	public void refreshNameTag(){
		String prefix = ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', PermissionsExManager.getShortPrefix(getPlayer())));

		
		String suffix = "&f";
		
		if(getTeam() != null){
			switch(getTeam().getType()){
				case ARMY_OF_THE_DEAD:
					suffix = "&c";
					break;
				case NIGHT_WATCH:
					suffix = "&a";
					break;
			}
			
		}
		
		NametagAPI.setPrefix(getName(), "&7"+prefix+suffix);
	}



	public ChatColor getColor(){
		if(getTeam() != null){
			return getTeam().getColor();
		}else{
			return ChatColor.WHITE;
		}
	}

}
