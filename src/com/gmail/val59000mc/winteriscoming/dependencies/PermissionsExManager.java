package com.gmail.val59000mc.winteriscoming.dependencies;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class PermissionsExManager {
    
	public static String getPrefix(Player player){
		 PermissionUser user = PermissionsEx.getUser(player);
		 return user.getPrefix();
	}
    
	public static String getSuffix(Player player){
		 PermissionUser user = PermissionsEx.getUser(player);
		 return user.getSuffix();
	}

	public static String getShortPrefix(Player player) {
		String prefix = getPrefix(player);
		prefix = prefix.replace("Modérateur", "Modo").replace("Administrateur", "Admin");
		return prefix;
	}

	public static String getStrippedPrefix(Player player) {
		return ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', getPrefix(player)));
	}
}
