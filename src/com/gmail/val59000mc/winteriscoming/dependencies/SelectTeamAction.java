package com.gmail.val59000mc.winteriscoming.dependencies;

import org.bukkit.Sound;
import org.bukkit.entity.Player;

import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;
import com.gmail.val59000mc.spigotutils.Sounds;
import com.gmail.val59000mc.winteriscoming.game.GameManager;
import com.gmail.val59000mc.winteriscoming.game.GameState;
import com.gmail.val59000mc.winteriscoming.players.PlayersManager;
import com.gmail.val59000mc.winteriscoming.players.TeamType;
import com.gmail.val59000mc.winteriscoming.players.WicPlayer;
import com.gmail.val59000mc.winteriscoming.players.WicTeam;

public class SelectTeamAction extends Action{
	
	private String team;
	
	public SelectTeamAction(String team){
		this.team = team;
	}

	@Override
	public void executeAction(Player player, SigPlayer sigPlayer) {

		if(team.equals("leave")){
			leaveTeam(player,sigPlayer);
		}else{
			selectTeam(player,sigPlayer);
		}
		
		
	}

	private void selectTeam(Player player, SigPlayer sigPlayer) {
		
		TeamType type = TeamType.valueOf(team);
		
		if(GameManager.instance().isState(GameState.WAITING)){

			PlayersManager pm = PlayersManager.instance();
			WicPlayer wicPlayer = pm.getWicPlayer(player);
			
			if(wicPlayer != null){
				WicTeam oldTeam = wicPlayer.getTeam();
				wicPlayer.leaveTeam();
				
				WicTeam newTeam = PlayersManager.instance().getTeam(type);
				
				if(newTeam.canAddPlayer()){
					newTeam.addPlayer(wicPlayer);
					wicPlayer.refreshNameTag();
					executeNextAction(player, sigPlayer, true);
					return;
				}else{
					if(oldTeam != null){
						oldTeam.addPlayer(wicPlayer);
					}
					wicPlayer.sendI18nMessage("team.full");
					Sounds.play(player, Sound.VILLAGER_NO, 1, 2);
					executeNextAction(player, sigPlayer, false);
					return;
				}
			}
		}
		
		executeNextAction(player, sigPlayer, false);
	}
	

	private void leaveTeam(Player player, SigPlayer sigPlayer) {
		
		if(GameManager.instance().isState(GameState.WAITING)){

			PlayersManager pm = PlayersManager.instance();
			WicPlayer uPlayer = pm.getWicPlayer(player);
			
			if(uPlayer != null){
				uPlayer.leaveTeam();
				uPlayer.refreshNameTag();
				executeNextAction(player, sigPlayer, true);
			}
		}
		
		executeNextAction(player, sigPlayer, false);
	}
}
