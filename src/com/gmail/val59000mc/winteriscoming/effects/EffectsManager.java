package com.gmail.val59000mc.winteriscoming.effects;

import org.bukkit.Location;

import de.slikey.effectlib.EffectLib;
import de.slikey.effectlib.EffectManager;
import de.slikey.effectlib.effect.LineEffect;
import de.slikey.effectlib.util.ParticleEffect;

public class EffectsManager {
	
	private static EffectManager effectManager;
	
	private static EffectManager getEffectManager(){
		if(effectManager == null){
			EffectLib lib = EffectLib.instance();
			effectManager = new EffectManager(lib);
		}
		return effectManager;
	}
	
	public static void drawLine(Location start, Location end){
		LineEffect lineEffect = new LineEffect(getEffectManager());
		lineEffect.visibleRange=40000;
		lineEffect.setLocation(start);
		lineEffect.setTarget(end);
		lineEffect.particle=ParticleEffect.FLAME;
		lineEffect.particles=300;
		lineEffect.iterations=300;
		lineEffect.period=20;
		lineEffect.start();
	}
	
}
