package com.gmail.val59000mc.winteriscoming.classes;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Horse;
import org.bukkit.inventory.ItemStack;

import com.gmail.val59000mc.winteriscoming.WIC;
import com.gmail.val59000mc.winteriscoming.i18n.I18n;
import com.gmail.val59000mc.winteriscoming.players.PlayersManager;
import com.gmail.val59000mc.winteriscoming.players.WicPlayer;
import com.gmail.val59000mc.winteriscoming.players.WicTeam;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Parser;
import com.gmail.val59000mc.spigotutils.Randoms;
import com.gmail.val59000mc.spigotutils.Reflection;
import com.gmail.val59000mc.spigotutils.Reflection.PackageType;

public class PlayerClassManager {
	
	
	private static PlayerClassManager instance;
	private List<PlayerClass> attackClasses;
	private List<PlayerClass> defenseClasses;

	public static PlayerClassManager instance(){
		if(instance == null){
			instance = new PlayerClassManager();
		}
		
		return instance;
	}
	
	private PlayerClassManager(){
		this.attackClasses = Collections.synchronizedList(new ArrayList<PlayerClass>());
		this.defenseClasses = Collections.synchronizedList(new ArrayList<PlayerClass>());
	}
	
	public static void load(){
		
		PlayerClassManager instance = PlayerClassManager.instance();

		// defense
		instance.getDefenseClasses().add(instance.parseWarriorClass(PlayerClassType.SOLDIER));
		instance.getDefenseClasses().add(instance.parseWarriorClass(PlayerClassType.ARCHER));
		instance.getDefenseClasses().add(instance.parseWarriorClass(PlayerClassType.LORD_COMMANDER));
		instance.getDefenseClasses().add(instance.parseWarriorClass(PlayerClassType.MESTRE));
	
		// attack
		instance.getAttackClasses().add(instance.parseWarriorClass(PlayerClassType.GIANT));
		instance.getAttackClasses().add(instance.parseWarriorClass(PlayerClassType.SKELETON));
		instance.getAttackClasses().add(instance.parseWarriorClass(PlayerClassType.WHITE_WALKER));
		instance.getAttackClasses().add(instance.parseWarriorClass(PlayerClassType.ZOMBIE));

	
	}
	
	private PlayerClass parseWarriorClass(PlayerClassType type){
		Logger.debug("-> PlayerClass::parseWarriorClass, type="+type);
		
		ConfigurationSection section = WIC.getPlugin().getConfig().getConfigurationSection("classes."+type.toString());
		
		if(section == null){
			Logger.severeC(I18n.get("parser.playerclass.not-found"));
			return null;
		}
		
		List<ItemStack> items = new ArrayList<ItemStack>();
		List<String> itemsStrList = section.getStringList("items");
		if(itemsStrList == null){
			Logger.severeC(I18n.get("parser.playerclass.items-not-found"));
			return null;
		}
		for(String itemStr : itemsStrList){
			items.add(Parser.parseItemStack(itemStr));
		}
		
		List<ItemStack> armor = new ArrayList<ItemStack>();
		List<String> armorStrList = section.getStringList("armor");
		if(armorStrList == null){
			Logger.severeC(I18n.get("parser.playerclass.armor-not-found"));
			return null;
		}
		for(String itemStr : armorStrList){
			armor.add(Parser.parseItemStack(itemStr));
		}
		
		Integer limit = section.getInt("limit",5);
		
		PlayerClass playerClass;
		
		try {
			playerClass = (PlayerClass) Reflection.instantiateObject(type.getClassName(), PackageType.WINTER_IS_COMING_PLAYER_CLASS, type, items, armor, limit);
			Logger.debug("<- PlayerClass::parseWarriorClass");
			return playerClass;
		} catch (InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException
				| NoSuchMethodException | ClassNotFoundException e) {
			Logger.severeC(I18n.get("parser.playerclass.wrong-player-class"));			
			Logger.debug("<- PlayerClass::parseWarriorClass");
			e.printStackTrace();
			return null;
		}
		

	}
	
	public void assignRandomAvailableClassToPlayer(WicPlayer wicPlayer){
		Logger.debug("-> PlayerClassManager::assignRandomAvailableClassToPlayer, player="+wicPlayer);
		PlayerClass playerClass = wicPlayer.getPlayerClass();
		if(playerClass != null){
			playerClass.unapplyLogic(wicPlayer);
			playerClass = null;
			wicPlayer.setPlayerClass(null);
		}
		
		List<PlayerClass> availableClasses = getAvailablePlayerClasses(wicPlayer);
		playerClass = availableClasses.get(Randoms.randomInteger(0, availableClasses.size() - 1));
		wicPlayer.setPlayerClass(playerClass);
		playerClass.applyLogic(wicPlayer);
		Logger.debug("<- PlayerClassManager::assignRandomAvailableClassToPlayer");
	}
	
	private List<PlayerClass> getAvailablePlayerClasses(WicPlayer wicPlayer){
		WicTeam team = null;
		Map<PlayerClass,Integer> remainingAvailableClasses = new HashMap<PlayerClass,Integer>();
		
		// Finding player classes of the correct team
		switch(wicPlayer.getTeam().getType()){
			case ARMY_OF_THE_DEAD:
				team = PlayersManager.instance().getAttackTeam();
				for(PlayerClass attackPlayerClass : getAttackClasses()){
					remainingAvailableClasses.put(attackPlayerClass, attackPlayerClass.getLimit());
				}
				break;
			case NIGHT_WATCH:
				team = PlayersManager.instance().getDefenseTeam();
				for(PlayerClass defensePlayerClass : getDefenseClasses()){
					remainingAvailableClasses.put(defensePlayerClass, defensePlayerClass.getLimit());
				}
				break;
		}
		
		// Calculating remaining available classes
		for(WicPlayer wicP : team.getMembers()){
			PlayerClass playerClass = wicP.getPlayerClass();
			
			if(playerClass != null){
				if(remainingAvailableClasses.containsKey(playerClass)){
					if(remainingAvailableClasses.get(playerClass) >= 1){
						remainingAvailableClasses.put(playerClass, remainingAvailableClasses.get(playerClass) - 1);
					}
				}else{
					remainingAvailableClasses.put(playerClass, playerClass.getLimit() - 1);
				}
			}
		}
		
		// Building returned list
		List<PlayerClass> availableClasses = new ArrayList<PlayerClass>();
		for(Entry<PlayerClass,Integer> entry : remainingAvailableClasses.entrySet()){
			if(entry.getValue() >= 1 && !availableClasses.contains(entry.getKey())){
				availableClasses.add(entry.getKey());
			}
		}
		
		return availableClasses;
	}
	
	private synchronized List<PlayerClass> getAttackClasses(){
		return attackClasses;
	}
	private synchronized List<PlayerClass> getDefenseClasses(){
		return defenseClasses;
	}
	
	private PlayerClass getPlayerClass(PlayerClassType type){
		for(PlayerClass playerClass : getAttackClasses()){
			if(playerClass.getType().equals(type)){
				return playerClass;
			}
		}
		for(PlayerClass playerClass : getDefenseClasses()){
			if(playerClass.getType().equals(type)){
				return playerClass;
			}
		}
		return null;
	}

	public boolean isWhiteWalkerHorse(WicPlayer wicPlayer, Horse horse) {
		PlayerClass whiteWalker = getPlayerClass(PlayerClassType.WHITE_WALKER);
		if(whiteWalker != null){
			Horse horseFound = ((WhiteWalker) whiteWalker).getHorse(wicPlayer);
			if(horseFound != null && horseFound.getUniqueId().equals(horse.getUniqueId())){
				return true;
			}
		}
		return false;
	}
}
