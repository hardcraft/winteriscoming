package com.gmail.val59000mc.winteriscoming.classes;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Horse;
import org.bukkit.entity.Horse.Variant;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

import com.gmail.val59000mc.spigotutils.Effects;
import com.gmail.val59000mc.spigotutils.Entities;
import com.gmail.val59000mc.spigotutils.Logger;
import com.gmail.val59000mc.spigotutils.Sounds;
import com.gmail.val59000mc.winteriscoming.WIC;
import com.gmail.val59000mc.winteriscoming.configuration.Config;
import com.gmail.val59000mc.winteriscoming.players.WicPlayer;


public class WhiteWalker extends PlayerClass{

	private Map<String,UUID> whiteWalkerHorses;
	
	public WhiteWalker(PlayerClassType type, List<ItemStack> items, List<ItemStack> armor, int limit) {
		super(type,items,armor,limit);
		whiteWalkerHorses = Collections.synchronizedMap(new HashMap<String,UUID>());
	}

	@Override
	public void applyLogic(WicPlayer wicPlayer) {
		super.applyLogic(wicPlayer);
		if(wicPlayer.isOnline()){
			Player player = wicPlayer.getPlayer();
			spawnAndMountHorse(player);
			Sounds.play(player, Sound.HORSE_SKELETON_IDLE, 1, 1.1f);
			Effects.addPermanent(player, PotionEffectType.NIGHT_VISION, 1);
		}
		
	}

	@Override
	public void unapplyLogic(WicPlayer wicPlayer) {
		// Kill horse
		Horse horse = getHorse(wicPlayer);
		if(horse != null){
			horse.getInventory().setArmor(null);
			horse.getInventory().setSaddle(null);
			horse.setHealth(0);
		}
		
	}
	
	private void spawnAndMountHorse(final Player player){
		Logger.debug("-> WhiteWalker::spawnHorse");
		
		Location location = player.getLocation().clone();
		final Horse horse = (Horse) location.getWorld().spawnEntity(location.clone(), EntityType.HORSE);
		whiteWalkerHorses.put(player.getName(), horse.getUniqueId());
		horse.setVariant(Variant.SKELETON_HORSE);
		horse.setAdult();
		horse.setJumpStrength(1);
		horse.setMaxHealth(40);
		horse.setHealth(40);
		horse.setOwner(player);
		horse.getInventory().setSaddle(new ItemStack(Material.SADDLE));
		horse.setCustomName(ChatColor.GRAY+"Monture de "+player.getName());
		
		Bukkit.getScheduler().runTaskLater(WIC.getPlugin(), new Runnable() {
			
			@Override
			public void run() {
				horse.setPassenger(player);
			}
		}, 1);
		
		Logger.debug("<- WhiteWalker::spawnHorse");
	}
	
	public Horse getHorse(WicPlayer wicPlayer){
		World world = Bukkit.getWorld(Config.worldName);
		return (Horse) Entities.get(world, whiteWalkerHorses.get(wicPlayer.getName()));
	}

}
