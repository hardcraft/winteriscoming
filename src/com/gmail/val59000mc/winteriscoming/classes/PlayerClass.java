package com.gmail.val59000mc.winteriscoming.classes;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

import com.gmail.val59000mc.winteriscoming.players.WicPlayer;
import com.gmail.val59000mc.spigotutils.Effects;
import com.gmail.val59000mc.spigotutils.Inventories;

public abstract class PlayerClass {
	private PlayerClassType type;
	private List<ItemStack> items;
	private List<ItemStack> armor;
	private int limit;
	
	public PlayerClass(PlayerClassType type, List<ItemStack> items, List<ItemStack> armor, int limit) {
		super();
		this.type = type;
		this.items = items;
		this.armor = armor;
		this.limit = limit;
	}

	public PlayerClassType getType() {
		return type;
	}

	public List<ItemStack> getItems() {
		return items;
	}

	public List<ItemStack> getArmor() {
		return armor;
	}

	public int getLimit() {
		return limit;
	}
	
	public void applyLogic(WicPlayer wicPlayer){
		if(wicPlayer.isOnline()){
			Player player = wicPlayer.getPlayer();
			player.setMaxHealth(20);
			Effects.clear(player);
			Effects.add(player, PotionEffectType.DAMAGE_RESISTANCE, 50, 200);
			Inventories.clear(player);
			Inventories.give(player,items);
			Inventories.setArmor(player,armor);
		}
	}
	
	public void unapplyLogic(WicPlayer wicPlayer){
		
	}
}
