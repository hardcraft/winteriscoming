package com.gmail.val59000mc.winteriscoming.classes;

import java.util.List;

import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

import com.gmail.val59000mc.spigotutils.Effects;
import com.gmail.val59000mc.spigotutils.Sounds;
import com.gmail.val59000mc.winteriscoming.players.WicPlayer;


public class Archer extends PlayerClass{

	public Archer(PlayerClassType type, List<ItemStack> items, List<ItemStack> armor, int limit) {
		super(type,items,armor,limit);
	}

	@Override
	public void applyLogic(WicPlayer wicPlayer) {
		super.applyLogic(wicPlayer);
		if(wicPlayer.isOnline()){
			Player player = wicPlayer.getPlayer();
			Effects.addPermanent(player, PotionEffectType.SPEED, 1);
			Sounds.play(player, Sound.ARROW_HIT, 1, 1);
		}
		
	}

	@Override
	public void unapplyLogic(WicPlayer wicPlayer) {
		// TODO Auto-generated method stub
		
	}

}
