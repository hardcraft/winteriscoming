package com.gmail.val59000mc.winteriscoming.classes;

import java.util.List;

import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

import com.gmail.val59000mc.spigotutils.Effects;
import com.gmail.val59000mc.spigotutils.Sounds;
import com.gmail.val59000mc.winteriscoming.players.WicPlayer;


public class Giant extends PlayerClass{

	public Giant(PlayerClassType type, List<ItemStack> items, List<ItemStack> armor, int limit) {
		super(type,items,armor,limit);
	}

	@Override
	public void applyLogic(WicPlayer wicPlayer) {
		super.applyLogic(wicPlayer);
		if(wicPlayer.isOnline()){
			Player player = wicPlayer.getPlayer();
			Effects.addPermanent(player, PotionEffectType.SLOW, 0);
			player.setMaxHealth(60);
			player.setHealth(60);
			Sounds.play(player, Sound.ZOMBIE_IDLE, 1, 0.5f);
		}
		
	}

	@Override
	public void unapplyLogic(WicPlayer wicPlayer) {
		// TODO Auto-generated method stub
		
	}

}
