package com.gmail.val59000mc.winteriscoming.classes;

import com.gmail.val59000mc.winteriscoming.configuration.Config;

public enum PlayerClassType {
	LORD_COMMANDER("player-class.lord-commander."+Config.map,"LordCommander"),
	MESTRE("player-class.mestre."+Config.map,"Mestre"),
	SOLDIER("player-class.soldier."+Config.map,"Soldier"),
	ARCHER("player-class.archer."+Config.map,"Archer"),
	ZOMBIE("player-class.zombie."+Config.map,"Zombie"),
	WHITE_WALKER("player-class.white-walker."+Config.map,"WhiteWalker"),
	GIANT("player-class.giant."+Config.map,"Giant"),
	SKELETON("player-class.skeleton."+Config.map,"Skeleton");
	
	private String className;
	private String code;

    private PlayerClassType(String code, String className){
    	this.code = code;
    	this.className = className;
    }

    public String getClassName() {
        return className;
    }
    
    public String getCode(){
    	return code;
    }
}
