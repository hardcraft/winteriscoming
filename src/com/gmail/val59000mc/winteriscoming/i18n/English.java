package com.gmail.val59000mc.winteriscoming.i18n;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.ChatColor;

public class English {

	public static Map<String, String> load() {
		Map<String,String> s = new HashMap<String,String>();

		
		// config
		s.put("config.dependency.vault-not-found", "Plugin Vault not found, no support for rewards");
		s.put("config.dependency.vault-loaded", "Plugin Vault loaded.");
		s.put("config.dependency.worldedit-not-found", "Plugin WorldEdit not found, no support for schematics");
		s.put("config.dependency.worldedit-loaded", "Plugin WorldEdit loaded.");
		s.put("config.dependency.libsdisguise-not-found", "Plugin LibsDisguises not found, no support for disguises");
		s.put("config.dependency.libsdisguise-loaded", "Plugin LibsDisguises loaded.");
		s.put("config.dependency.effectlib-not-found", "Plugin EffectLib not found, no support for effects");
		s.put("config.dependency.effectlib-loaded", "Plugin EffectLib loaded.");
		s.put("config.dependency.sig-not-found", "Plugin SimpleInventoryGUI not found, no support for this plugin");
		s.put("config.dependency.sig-loaded", "Plugin SimpleInventoryGUI loaded.");
		s.put("config.dependency.bountifulapi-not-found", "Plugin BountifulAPI not found, no support for titles");
		s.put("config.dependency.bountifulapi-loaded", "Plugin BountifulAPI loaded.");
		
		// commands
		s.put("command.global-chat.true", "§aYou are now talking to everyone.");
		s.put("command.global-chat.false", "§aYou are now talking to your team.");
		s.put("command.start.not-possible", "§cYou can't force starting the game.");
		s.put("command.start.ok", "§aGame starting has been forced.");
		
	    // parser
		s.put("parser.wrong-location", "Couldn't parser the location %location%");
		
		s.put("parser.item.not-found", "Couldn't find item string");
		s.put("parser.item.empty-string", "Item string is empty");
		s.put("parser.item.wrong-material", "The material name doesn't exist");
		s.put("parser.item.wrong-damage-value", "Wrong damage value");
		s.put("parser.item.wrong-amount", "Wrong amount");
		s.put("parser.item.wrong-enchantment-syntax", "Wrong enchantment syntax");
		s.put("parser.item.wrong-enchantment-name", "Wrong enchantment name");
		s.put("parser.item.wrong-enchantment-level", "Wrong enchantment level");
		
		s.put("parser.playerclass.not-found", "Player class not found");
		s.put("parser.playerclass.items-not-found", "items not found in player class");
		s.put("parser.playerclass.wrong-player-class", "Cannot instantiate player class");
		
					
		// map loader
		s.put("map-loader.load.no-last-world", "No last world to load, creating a new world");
		s.put("map-loader.load.last-world-not-found", "Last world not found, creating a new world.");
		s.put("map-loader.delete.no-last-world", "Not last world to delete.");
		s.put("map-loader.delete.last-world-not-found", "Last world to delete not found.");
		s.put("map-loader.delete", "Deleting last world.");
		s.put("map-loader.copy.not-found", "'winteriscoming' directory not found, cannot copy.");

		// game
		s.put("game.player-allowed-to-join", "Players are now allowed to join");
		s.put("game.start", "The game begins !");
		s.put("game.starting-in", "The game will begin in %time% !");
		s.put("game.end-in", "The game will end in %time ! because there are not enough players left.");
		s.put("game.end", "The game is finished !");
		s.put("game.end-stopped", "The game continues !");
		s.put("game.shutting-down-in", "Shutting down in %time% !");
		s.put("game.remaining-time", "Time left : %time% !");
		
		s.put("game.end.attack-win.game-of-thrones", ChatColor.GREEN+"The Army of the Dead won the game !");
		s.put("game.end.defense-win.game-of-thrones", ChatColor.GREEN+"The Night's Watch won the game !");
		
		s.put("game.end.attack-win.lord-of-the-rings", ChatColor.GREEN+"The Mordor won the game !");
		s.put("game.end.defense-win.lord-of-the-rings", ChatColor.GREEN+"The Middle Earth won the game !");
		
		s.put("game.end.no-more-players", ChatColor.GREEN+"Game has ended because there were no more players left.");
		s.put("game.leave-arena", ChatColor.RED+"Don't try to leave the map.");
		
		// players
		s.put("player.not-allowed-to-join", "You are not allowed to join that game");
		s.put("player.full", "The game if full. If nobody logs out you will be a spectator !");
		s.put("player.joined", "§f%player% §ajoined the game §2§l[%count%/%total%]");
		s.put("player.welcome", "Welcome in 'Winter Is Coming' game !");
		s.put("player.spectate", "You are spectating.");
		s.put("player.coins-earned", ChatColor.GREEN+"HardCoins earned : ");
		s.put("player.kill-after-disconnect-warning", "§7%player% has disconnected. He has up to %time% to reconnect.");
		s.put("player.kill-after-disconnect", "§7%player% didn't log back in the game and has been eliminated.");
		s.put("player.reconnect", "§7%player% logged back in the game.");
		
		// player class
		s.put("player-class.lord-commander.game-of-thrones", "Lord Commander");
		s.put("player-class.mestre.game-of-thrones", "Mestre");
		s.put("player-class.soldier.game-of-thrones", "Soldier");
		s.put("player-class.archer.game-of-thrones", "Archer");
		s.put("player-class.zombie.game-of-thrones", "Zombie");
		s.put("player-class.white-walker.game-of-thrones", "White Walker");
		s.put("player-class.giant.game-of-thrones", "Wrecker");
		s.put("player-class.skeleton.game-of-thrones", "Skeleton");

		s.put("player-class.lord-commander.lord-of-the-rings", "Man");
		s.put("player-class.mestre.lord-of-the-rings", "Wizard");
		s.put("player-class.soldier.lord-of-the-rings", "Dwarf");
		s.put("player-class.archer.lord-of-the-rings", "Elf");
		s.put("player-class.zombie.lord-of-the-rings", "Orc");
		s.put("player-class.white-walker.lord-of-the-rings", "Nazgul");
		s.put("player-class.giant.lord-of-the-rings", "Troll");
		s.put("player-class.skeleton.lord-of-the-rings", "Uruk");
		
		s.put("stats.end", "[\"\",{text:\"]-----[ WIC game summary ]-----[\",bold:true,color:green},{text:\"\n\n\"},{text:\"  >>  Kills : \",bold:true,color:green},{text:\"%kills%\n\"},{text:\"  >>  Deaths : \",bold:true,color:green},{text:\"%deaths%\n\"},{text:\"  >>  HardCoins earned : \",bold:true,color:green},{text:\"%hardcoins%\n\"},{text:\"  >>  Winning team : \",bold:true,color:green},{text:\"%winner%\n\n\"},{text:\"]----------------------------------------[\",color:green}]");
		s.put("stats.end.draw", "[\"\",{text:\"]-----[ WIC game summary ]-----[\",bold:true,color:green},{text:\"\n\n\"},{text:\"  >>  Kills : \",bold:true,color:green},{text:\"%kills%\n\"},{text:\"  >>  Deaths : \",bold:true,color:green},{text:\"%deaths%\n\"},{text:\"  >>  HardCoins earned : \",bold:true,color:green},{text:\"%hardcoins%\n\"},{text:\"  >>  Winning team : \",bold:true,color:green},{text:\"Draw\n\n\"},{text:\"]----------------------------------------[\",color:green}]");
		s.put("stats.death", "[\"\",{text:\"]-----[ You are dead ]-----[\",bold:true,color:green},{text:\"\n\n\"},{text:\"  >>  Kills : \",bold:true,color:green},{text:\"%kills%\n\"},{text:\"  >>  Deaths : \",bold:true,color:green},{text:\"%deaths%\n\"},{text:\"  >>  HardCoins earned : \",bold:true,color:green},{text:\"%hardcoins%\n\"},{text:\"\n\n\"},{text:\"]----------------------------------------[\",color:green}]");

		// team
		s.put("team.ARMY_OF_THE_DEAD.game-of-thrones", "Army of the Dead");
		s.put("team.NIGHT_WATCH.game-of-thrones", "Night's watch");
		
		s.put("team.ARMY_OF_THE_DEAD.lord-of-the-rings", "Mordor");
		s.put("team.NIGHT_WATCH.lord-of-the-rings", "Middle Earth");
		
		s.put("team.ARMY_OF_THE_DEAD.game-of-thrones.side", "Attack the beacons");
		s.put("team.NIGHT_WATCH.game-of-thrones.side", "Defend the beacons");
		
		s.put("team.ARMY_OF_THE_DEAD.lord-of-the-rings.side", "Attack the beacons");
		s.put("team.NIGHT_WATCH.lord-of-the-rings.side", "Defend the beacons");

		s.put("team.full", "§cThere are already too many players in that team.");
		
		// scoreboard
		s.put("scoreboard.team", "Team");
		s.put("scoreboard.kills-deaths", "Kills / Deaths");
		s.put("scoreboard.time-left", "Time left");
		s.put("scoreboard.objectives", "Objectives");
		s.put("scoreboard.player-class", "Class");
		s.put("scoreboard.coins-earned", "Coins");
		s.put("scoreboard.online-players", "Players");
		s.put("scoreboard.time-to-start", "Starting in");
		
		// objectives
		s.put("objectives.not-found", "3 Objectives locations not found");
		
		s.put("objectives.fence.game-of-thrones", "Fence");
		s.put("objectives.forest.game-of-thrones", "Forest");
		s.put("objectives.wall.game-of-thrones", "Wall");
		
		s.put("objectives.fence.lord-of-the-rings", "Cathedral");
		s.put("objectives.forest.lord-of-the-rings", "Plains");
		s.put("objectives.wall.lord-of-the-rings", "Mordor");
		
		s.put("objectives.cannot-break-other-level", ChatColor.RED+"You must break the previous objectives first.");
		s.put("objectives.cannot-break-team-grief", ChatColor.RED+"Only attackers can break objectives.");
		s.put("objectives.broken-by", ChatColor.YELLOW+"%player% broke an objective");
		s.put("objectives.defense-line-retreat", ChatColor.GOLD+"The defense line is overwhelmed, retreat !");
		s.put("objectives.defense-line-breached", ChatColor.GOLD+"The defense line is breached, continue the fight !");
		
		s.put("objectives.defend.2.game-of-thrones", ChatColor.RED+"Defend the Forest !");
		s.put("objectives.defend.3.game-of-thrones", ChatColor.RED+"Defend the Wall !");
		s.put("objectives.attack.2.game-of-thrones", ChatColor.RED+"Attack the Forest !");
		s.put("objectives.attack.3.game-of-thrones", ChatColor.RED+"Attack the Wall !");
		
		s.put("objectives.defend.2.lord-of-the-rings", ChatColor.RED+"Defend the Plains !");
		s.put("objectives.defend.3.lord-of-the-rings", ChatColor.RED+"Defend the Mordor !");
		s.put("objectives.attack.2.lord-of-the-rings", ChatColor.RED+"Attack the Plains !");
		s.put("objectives.attack.3.lord-of-the-rings", ChatColor.RED+"Attack the Mordor !");
		
		// bridge
		s.put("bridge.explode-in.game-of-thrones", ChatColor.YELLOW+"The bridge will explode in %time%");
		s.put("bridge.explode-in.lord-of-the-rings", ChatColor.YELLOW+"The stairs will explode in %time%");
		
		// ping
		s.put("ping.loading", "Loading");
		s.put("ping.playing", "Playing");
		s.put("ping.starting", "Starting");
		s.put("ping.waiting", "Waiting");
		s.put("ping.ended", "Ended");
				
				
		return s;
	}

}
