package com.gmail.val59000mc.winteriscoming.i18n;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.ChatColor;

public class French {

	public static Map<String, String> load() {
		Map<String,String> s = new HashMap<String,String>();
		
		// config
		s.put("config.dependency.vault-not-found", "Le plugin Vault est introuvable, pas de support pour les recompenses");
		s.put("config.dependency.vault-loaded", "Le plugin Vault a ete correctement charge.");
		s.put("config.dependency.worldedit-not-found", "Le plugin WorldEdit est introuvable, pas de support pour les schematics");
		s.put("config.dependency.worldedit-loaded", "Le plugin WorldEdit a ete correctement charge.");
		s.put("config.dependency.libsdisguise-not-found", "Le plugin LibsDisguises est introuvable, pas de support pour les deguisements");
		s.put("config.dependency.libsdisguise-loaded", "Le plugin LibsDisguises a ete correctement charge.");
		s.put("config.dependency.effectlib-not-found", "Le plugin EffectLib est introuvable, pas de support pour les effets");
		s.put("config.dependency.effectlib-loaded", "Le plugin EffectLib a ete correctement charge.");
		s.put("config.dependency.sig-not-found", "Le plugin SimpleInventoryGUI est introuvable, pas de support pour ce plugin");
		s.put("config.dependency.sig-loaded", "Le plugin SimpleInventoryGUI a ete correctement charge.");
		s.put("config.dependency.bountifulapi-not-found", "Le plugin BountifulAPI est introuvable, pas de support pour les titres");
		s.put("config.dependency.bountifulapi-loaded", "Le plugin BountifulAPI a ete correctement charge.");
		
		// commands
		s.put("command.global-chat.true", "§aTu parles désormais à tout le monde.");
		s.put("command.global-chat.false", "§aTu parles désormais à ta team.");
		s.put("command.start.not-possible", "§cTu ne peux pas forcer le démarrage du jeu.");
		s.put("command.start.ok", "§aLe lancement du jeu a été forcé.");
		
		// parser
		s.put("parser.wrong-location", "Impossible de parser la location %location%");
		
		s.put("parser.item.not-found", "Texte de config l'item non trouve");
		s.put("parser.item.empty-string", "Texte de config l'item vide");
		s.put("parser.item.wrong-material", "La materiau n'existe pas");
		s.put("parser.item.wrong-damage-value", "Erreur de damage value");
		s.put("parser.item.wrong-amount", "Erreur sur la quantite");
		s.put("parser.item.wrong-enchantment-syntax", "Mauvaise syntaxe d'enchantement");
		s.put("parser.item.wrong-enchantment-name", "Mauvais nom d'enchantement");
		s.put("parser.item.wrong-enchantment-level", "Mauvais niveau d'enchantement");
		
		s.put("parser.playerclass.not-found", "Classe de joueur introuvable");
		s.put("parser.playerclass.items-not-found", "items introuvables dans la classe de joueur");
		s.put("parser.playerclass.wrong-player-class", "impossible de creer la classe de joueur");
		
					
		// map loader
		s.put("map-loader.load.no-last-world", "Pas d'ancienne map a charger, creation d'une nouvelle map");
		s.put("map-loader.load.last-world-not-found", "Ancienne map non trouvee, creation d'une nouvelle map");
		s.put("map-loader.delete.no-last-world", "Pas d'ancienne map a supprimer.");
		s.put("map-loader.delete.last-world-not-found", "Ancienne map a supprimer non trouvee.");
		s.put("map-loader.delete", "Suppression de l'ancienne map.");
		s.put("map-loader.copy.not-found", "Dossier 'winteriscoming' introuvable, copie impossible.");
		
		// game
		s.put("game.player-allowed-to-join", "Les joueurs peuvent maintenant rejoindre la partie");
		s.put("game.start", "La partie commence !");
		s.put("game.starting-in", "La partie va commencer dans %time% !");
		s.put("game.end-in", "La partie va finir dans %time car il n'y a plus assez de joueurs.");
		s.put("game.end", "La partie est terminée !");
		s.put("game.end-stopped", "La partie continue !");
		s.put("game.shutting-down-in", "Arrêt dans %time% !");
		s.put("game.remaining-time", "Il reste %time% !");

		s.put("game.end.attack-win.game-of-thrones", ChatColor.GREEN+"L'Armee des Morts remporte la partie !");
		s.put("game.end.defense-win.game-of-thrones", ChatColor.GREEN+"La Garde de Nuit remporte la partie !");
		
		s.put("game.end.attack-win.lord-of-the-rings", ChatColor.GREEN+"Le Mordor remporte la partie !");
		s.put("game.end.defense-win.lord-of-the-rings", ChatColor.GREEN+"La Terre du Milieu remporte la partie !");
		
		s.put("game.end.no-more-players", ChatColor.GREEN+"Le jeu termine car il n'y a plus de joueurs.");
		s.put("game.leave-arena", ChatColor.RED+"N'essaie pas de sortir de la map.");

		// players
		s.put("player.not-allowed-to-join", "Vous n'etes pas autorise a rejoindre cette partie.");
		s.put("player.full", "La partie est pleine. Si personne ne se deconnecte, tu seras spectateur.");
		s.put("player.joined", "§f%player% §aa rejoint la partie §2§l[%count%/%total%]");
		s.put("player.welcome", "Bienvenue dans le jeu 'Winter Is Coming' !");
		s.put("player.spectate", "Tu es spectateur.");
		s.put("player.coins-earned", ChatColor.GREEN+"HardCoins gagnés : ");
		s.put("player.kill-after-disconnect-warning", "§7%player% s'est déconnecté. Il a jusque %time% pour se reconnecter.");
		s.put("player.kill-after-disconnect", "§7%player% ne s'est pas reconnecté et a été éliminé.");
		s.put("player.reconnect", "§7%player% s'est reconnecté.");
		
		// player class
		s.put("player-class.lord-commander.game-of-thrones", "Commandant en Chef");
		s.put("player-class.mestre.game-of-thrones", "Mestre");
		s.put("player-class.soldier.game-of-thrones", "Soldat");
		s.put("player-class.archer.game-of-thrones", "Archer");
		s.put("player-class.zombie.game-of-thrones", "Zombie");
		s.put("player-class.white-walker.game-of-thrones", "Marcheur Blanc");
		s.put("player-class.giant.game-of-thrones", "Demolisseur");
		s.put("player-class.skeleton.game-of-thrones", "Squelette");

		s.put("player-class.lord-commander.lord-of-the-rings", "Homme");
		s.put("player-class.mestre.lord-of-the-rings", "Magicien");
		s.put("player-class.soldier.lord-of-the-rings", "Nain");
		s.put("player-class.archer.lord-of-the-rings", "Elfe");
		s.put("player-class.zombie.lord-of-the-rings", "Orc");
		s.put("player-class.white-walker.lord-of-the-rings", "Nazgul");
		s.put("player-class.giant.lord-of-the-rings", "Troll");
		s.put("player-class.skeleton.lord-of-the-rings", "Uruk");
		
		s.put("stats.end", "[\"\",{text:\"]-------[ Résumé de la partie de WIC ]-------[\",bold:true,color:green},{text:\"\n\n\"},{text:\"  >>  Tués : \",bold:true,color:green},{text:\"%kills%\n\"},{text:\"  >>  Morts : \",bold:true,color:green},{text:\"%deaths%\n\"},{text:\"  >>  HardCoins gagnés : \",bold:true,color:green},{text:\"%hardcoins%\n\"},{text:\"  >>  Equipe gagnante : \",bold:true,color:green},{text:\"%winner%\n\n\"},{text:\"]----------------------------------------[\",color:green}]");
		s.put("stats.end.draw", "[\"\",{text:\"]-------[ Résumé de la partie de WIC ]-------[\",bold:true,color:green},{text:\"\n\n\"},{text:\"  >>  Tués : \",bold:true,color:green},{text:\"%kills%\n\"},{text:\"  >>  Morts : \",bold:true,color:green},{text:\"%deaths%\n\"},{text:\"  >>  HardCoins gagnés : \",bold:true,color:green},{text:\"%hardcoins%\n\"},{text:\"  >>  Equipe gagnante : \",bold:true,color:green},{text:\"Match nul\n\n\"},{text:\"]----------------------------------------[\",color:green}]");
		s.put("stats.death", "[\"\",{text:\"]-------------[ Tu es mort ]-----------[\",bold:true,color:green},{text:\"\n\n\"},{text:\"  >>  Tués : \",bold:true,color:green},{text:\"%kills%\n\"},{text:\"  >>  Morts : \",bold:true,color:green},{text:\"%deaths%\n\"},{text:\"  >>  HardCoins gagnés : \",bold:true,color:green},{text:\"%hardcoins%\n\"},{text:\"\n\n\"},{text:\"]----------------------------------------[\",color:green}]");

		
		// team
		s.put("team.ARMY_OF_THE_DEAD.game-of-thrones", "Armée des morts");
		s.put("team.NIGHT_WATCH.game-of-thrones", "Garde de nuit");
		
		s.put("team.ARMY_OF_THE_DEAD.lord-of-the-rings", "Mordor");
		s.put("team.NIGHT_WATCH.lord-of-the-rings", "Terre du Milieu");
		
		s.put("team.ARMY_OF_THE_DEAD.game-of-thrones.side", "Attaque les beacons");
		s.put("team.NIGHT_WATCH.game-of-thrones.side", "Defends les beacons");
		
		s.put("team.ARMY_OF_THE_DEAD.lord-of-the-rings.side", "Attaque les beacons");
		s.put("team.NIGHT_WATCH.lord-of-the-rings.side", "Defends les beacons");
		
		s.put("team.full", "§cIl y a déjà trop de joueurs dans cette équipe.");
		
		// scoreboard
		s.put("scoreboard.team", "Equipe");
		s.put("scoreboard.kills-deaths", "Tues / Morts");
		s.put("scoreboard.time-left", "Temps restant");
		s.put("scoreboard.objectives", "Objectifs");
		s.put("scoreboard.player-class", "Classe");
		s.put("scoreboard.coins-earned", "Coins");
		s.put("scoreboard.online-players", "Joueurs");
		s.put("scoreboard.time-to-start", "Commence dans");
		
		// objectives
		s.put("objectives.not-found", "3 locations d'objectives introuvables");
		
		s.put("objectives.fence.game-of-thrones", "Palissade");
		s.put("objectives.forest.game-of-thrones", "Foret");
		s.put("objectives.wall.game-of-thrones", "Mur");
		
		s.put("objectives.fence.lord-of-the-rings", "Cathédrale");
		s.put("objectives.forest.lord-of-the-rings", "Plaines");
		s.put("objectives.wall.lord-of-the-rings", "Mordor");
		
		s.put("objectives.cannot-break-other-level", ChatColor.RED+"Tu dois casser les objectifs précedents en premier.");
		s.put("objectives.cannot-break-team-grief", ChatColor.RED+"Seuls les attaquants peuvent casser les objectifs.");
		s.put("objectives.broken-by", ChatColor.YELLOW+"%player% a casse un objectif");
		s.put("objectives.defense-line-retreat", ChatColor.GOLD+"La ligne de defense est perdue, battez en retraite !");
		s.put("objectives.defense-line-breached", ChatColor.GOLD+"La ligne de defense est percée, continue le combat !");
		
		s.put("objectives.defend.2.game-of-thrones", ChatColor.GOLD+"Défends la Forêt !");
		s.put("objectives.defend.3.game-of-thrones", ChatColor.GOLD+"Défends le Mur !");
		s.put("objectives.attack.2.game-of-thrones", ChatColor.GOLD+"Attaque la Forêt !");
		s.put("objectives.attack.3.game-of-thrones", ChatColor.GOLD+"Attaque le Mur !");
		
		s.put("objectives.defend.2.lord-of-the-rings", ChatColor.GOLD+"Défends la Plaine !");
		s.put("objectives.defend.3.lord-of-the-rings", ChatColor.GOLD+"Défends le Mordor !");
		s.put("objectives.attack.2.lord-of-the-rings", ChatColor.GOLD+"Attaque la Plaine !");
		s.put("objectives.attack.3.lord-of-the-rings", ChatColor.GOLD+"Attaque le Mordor !");
		
		// bridge
		s.put("bridge.explode-in.game-of-thrones", ChatColor.YELLOW+"Le pont va exploser dans %time%");
		s.put("bridge.explode-in.lord-of-the-rings", ChatColor.YELLOW+"L'escalier va exploser dans %time%");
		
		// ping
		s.put("ping.loading", "Chargement");
		s.put("ping.playing", "En jeu");
		s.put("ping.starting", "Commence");
		s.put("ping.waiting", "En attente");
		s.put("ping.ended", "Fin");
		
		return s;
	}

}
